/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.budget.web;

import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.Matchers.any;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import guru.nidi.ramltester.junit.RamlMatchers;
import java.util.Collections;
import java.util.Optional;
import java.util.UUID;
import org.apache.http.HttpStatus;
import org.javers.core.commit.CommitId;
import org.javers.core.commit.CommitMetadata;
import org.javers.core.diff.changetype.ValueChange;
import org.javers.core.metamodel.object.GlobalId;
import org.javers.core.metamodel.object.UnboundedValueObjectId;
import org.javers.repository.jql.JqlQuery;
import org.joda.time.LocalDateTime;
import org.junit.Before;
import org.junit.Test;
import org.openlmis.budget.domain.RequisitionBudget;
import org.openlmis.budget.domain.RequisitionBudgetDataBuilder;
import org.openlmis.budget.i18n.MessageKeys;
import org.openlmis.budget.web.budget.RequisitionBudgetController;
import org.openlmis.budget.web.budget.RequisitionBudgetDto;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

@SuppressWarnings("PMD.TooManyMethods")
public class RequisitionBudgetControllerIntegrationTest extends BaseWebIntegrationTest {

  private static final String RESOURCE_URL
          = RequisitionBudgetController.RESOURCE_PATH;
  private static final String ID_URL = RESOURCE_URL + "/{id}";
  private static final String AUDIT_LOG_URL = ID_URL + "/auditLog";
  private UUID sourceOfFundId = UUID.fromString("78d42bdb-9150-4c52-bd77-e42d777cfaed");
  private UUID requisitionId = UUID.fromString("466b2e7f-5798-4027-a2ca-373627748ea6");

  private static final String NAME = "name";

  private RequisitionBudget requisitionBudget
          = new RequisitionBudgetDataBuilder().build();
  private RequisitionBudgetDto requisitionBudgetDto
          = RequisitionBudgetDto.newInstance(requisitionBudget);

  private GlobalId globalId = new UnboundedValueObjectId(RequisitionBudget.class.getSimpleName());
  private ValueChange change = new ValueChange(globalId, NAME, "name1", "name2");

  private CommitId commitId = new CommitId(1, 0);
  private CommitMetadata commitMetadata = new CommitMetadata(
            "admin", Maps.newHashMap(), LocalDateTime.now(), commitId);

  @Before
  public void setUp() {
    given(requisitionBudgetRepository
            .saveAndFlush(any(RequisitionBudget.class)))
            .willAnswer(new SaveAnswer<>());
    change.bindToCommit(commitMetadata);
  }

  @Test
  public void shouldReturnPageOfRequisitionBudgets() {
    given(requisitionBudgetRepository.findAll(any(Pageable.class)))
            .willReturn(new PageImpl<>(Collections.singletonList(requisitionBudget)));

    restAssured
            .given()
            .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
            .queryParam("page", pageable.getPageNumber())
            .queryParam("size", pageable.getPageSize())
            .when()
            .get(RESOURCE_URL)
            .then()
            .statusCode(HttpStatus.SC_OK)
            .body("content", hasSize(1))
            .body("content[0].sourceOfFundId",
                    is(requisitionBudget.getSourceOfFundId().toString()));
    assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(), RamlMatchers.hasNoViolations());
  }

  @Test
  public void shouldReturnUnauthorizedForRequisitionBudgetsEndpointIfUserIsNotAuthorized() {
    restAssured.given()
            .when()
            .get(RESOURCE_URL)
            .then()
            .statusCode(HttpStatus.SC_UNAUTHORIZED);

    assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(), RamlMatchers.hasNoViolations());
  }

  @Test
  public void postShouldCreateRequisitionBudget() {

    RequisitionBudgetDto response = restAssured
            .given()
            .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
            .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
            .body(requisitionBudgetDto)
            .when()
            .post(RESOURCE_URL)
            .then()
            .statusCode(201)
            .extract().as(RequisitionBudgetDto.class);

    assertEquals(sourceOfFundId, response.getSourceOfFundId());
    assertEquals(requisitionId, response.getRequisitionId());
    assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(), RamlMatchers.hasNoViolations());
  }

  @Test
  public void shouldReturnUnauthorizedForCreateBudgetRequisitionEndpointIfUserIsNotAuthorized() {
    restAssured
            .given()
            .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
            .body(requisitionBudgetDto)
            .when()
            .post(RESOURCE_URL)
            .then()
            .statusCode(HttpStatus.SC_UNAUTHORIZED);

    assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(), RamlMatchers.hasNoViolations());
  }

  @Test
  public void postShouldReturnBadRequestIfRequestedBodyIsInvalid() {
    requisitionBudgetDto.setRequisitionId(null);

    restAssured.given()
            .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
            .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
            .body(requisitionBudgetDto)
            .when()
            .post(RESOURCE_URL)
            .then()
            .statusCode(400)
            .body(MESSAGE_KEY, is(MessageKeys.MUST_CONTAIN_VALUE));

    // we don't check request body because of the purpose of the test
    assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(), RamlMatchers.validates());
    assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(), RamlMatchers.responseChecks());
  }

  @Test
  public void shouldReturnGivenRequisitionId() {
    given(requisitionBudgetRepository
            .findById(requisitionBudgetDto.getId()))
            .willReturn(Optional.of(requisitionBudget));

    restAssured
            .given()
            .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
            .pathParam(ID, requisitionBudgetDto
                    .getId().toString())
            .when()
            .get(ID_URL)
            .then()
            .statusCode(HttpStatus.SC_OK)
            .body(ID, is(requisitionBudgetDto
                    .getId().toString()));

    assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(), RamlMatchers.hasNoViolations());
  }

  @Test
  public void shouldReturnUnauthorizedForGetRequionBudgetEndpointIfUserIsNotAuthorized() {
    restAssured
            .given()
            .pathParam(ID, requisitionBudgetDto
                    .getId().toString())
            .when()
            .get(ID_URL)
            .then()
            .statusCode(HttpStatus.SC_UNAUTHORIZED);

    assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(), RamlMatchers.hasNoViolations());
  }

  @Test
  public void shouldUpdateRequisitionBudget() {
    given(requisitionBudgetRepository
            .findById(requisitionBudgetDto.getId()))
            .willReturn(Optional.of(requisitionBudget));
    restAssured
            .given()
            .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
            .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
            .pathParam(ID, requisitionBudgetDto.getId().toString())
            .body(requisitionBudgetDto)
            .when()
            .put(ID_URL)
            .then()
            .statusCode(HttpStatus.SC_OK)
            .body(ID, is(requisitionBudgetDto.getId().toString()));

    assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(), RamlMatchers.hasNoViolations());
  }

  @Test
  public void shouldReturnBadRequestMessageIfBudgetRequisitionCannotBeUpdated() {
    restAssured
            .given()
            .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
            .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
            .pathParam(ID, UUID.randomUUID().toString())
            .body(requisitionBudgetDto)
            .when()
            .put(ID_URL)
            .then()
            .statusCode(HttpStatus.SC_BAD_REQUEST)
            .body(MESSAGE_KEY, is(MessageKeys.ERROR_BUDGET_ID_MISMATCH));

    assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(), RamlMatchers.hasNoViolations());
  }

  @Test
  public void shouldReturnUnauthorizedForUpdateRequisitionBudgetEndpointIfUserIsNotAuthorized() {
    restAssured
            .given()
            .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
            .pathParam(ID, requisitionBudgetDto.getId().toString())
            .body(requisitionBudgetDto)
            .when()
            .put(ID_URL)
            .then()
            .statusCode(HttpStatus.SC_UNAUTHORIZED);

    assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(), RamlMatchers.hasNoViolations());
  }

  @Test
  public void shouldRetrieveAuditLogs() {
    given(requisitionBudgetRepository
            .existsById(requisitionBudgetDto.getId())).willReturn(true);
    willReturn(Lists.newArrayList(change)).given(javers).findChanges(any(JqlQuery.class));

    restAssured
            .given()
            .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
            .pathParam(ID, requisitionBudgetDto.getId().toString())
            .when()
            .get(AUDIT_LOG_URL)
            .then()
            .statusCode(HttpStatus.SC_OK)
            .body("", hasSize(1))
            .body("changeType", hasItem(change.getClass().getSimpleName()))
            .body("globalId.valueObject", hasItem(RequisitionBudget.class.getSimpleName()))
            .body("commitMetadata.author", hasItem(commitMetadata.getAuthor()))
            .body("commitMetadata.properties", hasItem(hasSize(0)))
            .body("commitMetadata.commitDate", hasItem(commitMetadata.getCommitDate().toString()))
            .body("commitMetadata.id", hasItem(commitId.valueAsNumber().floatValue()))
            .body("property", hasItem(change.getPropertyName()))
            .body("left", hasItem(change.getLeft().toString()))
            .body("right", hasItem(change.getRight().toString()));

    assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(), RamlMatchers.hasNoViolations());
  }

  @Test
  public void shouldRetrieveAuditLogsWithParameters() {
    given(requisitionBudgetRepository
            .existsById(requisitionBudgetDto.getId())).willReturn(true);
    willReturn(Lists.newArrayList(change)).given(javers).findChanges(any(JqlQuery.class));

    restAssured
            .given()
            .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
            .pathParam(ID, requisitionBudgetDto.getId().toString())
            .queryParam("author", commitMetadata.getAuthor())
            .queryParam("changedPropertyName", change.getPropertyName())
            .when()
            .get(AUDIT_LOG_URL)
            .then()
            .statusCode(HttpStatus.SC_OK)
            .body("", hasSize(1))
            .body("changeType", hasItem(change.getClass().getSimpleName()))
            .body("globalId.valueObject", hasItem(RequisitionBudget.class.getSimpleName()))
            .body("commitMetadata.author", hasItem(commitMetadata.getAuthor()))
            .body("commitMetadata.properties", hasItem(hasSize(0)))
            .body("commitMetadata.commitDate", hasItem(commitMetadata.getCommitDate().toString()))
            .body("commitMetadata.id", hasItem(commitId.valueAsNumber().floatValue()))
            .body("property", hasItem(change.getPropertyName()))
            .body("left", hasItem(change.getLeft().toString()))
            .body("right", hasItem(change.getRight().toString()));

    assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(), RamlMatchers.hasNoViolations());
  }

  @Test
  public void shouldReturnNotFoundMessageIfRequisitionBudgetDoesNotExistForAuditLogEndpoint() {
    given(requisitionBudgetRepository
            .existsById(requisitionBudgetDto.getId())).willReturn(false);

    restAssured
            .given()
            .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
            .pathParam(ID, requisitionBudgetDto.getId().toString())
            .when()
            .get(AUDIT_LOG_URL)
            .then()
            .statusCode(404);

    assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(), RamlMatchers.hasNoViolations());
  }

  @Test
  public void getShouldReturnNotFoundForNonExistingRequisitionBudget() {
    given(requisitionBudgetRepository
            .findById(requisitionId)).willReturn(Optional.empty());

    restAssured
            .given()
            .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
            .pathParam("id", requisitionBudgetDto.getId().toString())
            .when()
            .get(ID_URL)
            .then()
            .statusCode(404)
            .body(MESSAGE_KEY, is(MessageKeys.ERROR_REQUISITION_BUDGET_NOT_FOUND));

    assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(), RamlMatchers.hasNoViolations());
  }

  @Test
  public void shouldReturnUnauthorizedForAuditLogEndpointIfUserIsNotAuthorized() {
    restAssured
            .given()
            .pathParam(ID, requisitionBudgetDto.getId().toString())
            .when()
            .get(AUDIT_LOG_URL)
            .then()
            .statusCode(HttpStatus.SC_UNAUTHORIZED);

    assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(), RamlMatchers.hasNoViolations());
  }

}
