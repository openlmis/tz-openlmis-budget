/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.budget.web;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.openlmis.budget.i18n.MessageKeys.ERROR_CODE_OR_NAME_REQUIRED;
import static org.powermock.api.mockito.PowerMockito.when;

import com.jayway.restassured.response.ValidatableResponse;
import guru.nidi.ramltester.junit.RamlMatchers;
import java.util.Collections;
import java.util.Optional;
import java.util.UUID;
import net.sf.jasperreports.engine.JRException;
import org.apache.http.HttpStatus;
import org.hamcrest.Matcher;
import org.junit.Before;
import org.junit.Test;
import org.openlmis.budget.SourceOfFundDataBuilder;
import org.openlmis.budget.domain.SourceOfFund;
import org.openlmis.budget.exception.ValidationMessageException;
import org.openlmis.budget.i18n.MessageKeys;
import org.openlmis.budget.web.budget.SourceOfFundDto;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

@SuppressWarnings({"PMD.TooManyMethods"})
public class SourceOfFundControllerIntegrationTest extends BaseWebIntegrationTest {

  private static final String REPORT_FORMAT_CSV = "csv";
  private static final String REPORT_FORMAT_PDF = "pdf";
  private static final String REPORT_FORMAT_XLS = "xls";
  private static final String REPORT_FORMAT_HTML = "html";

  private static final String RESOURCE_URL = "/api/sourceOfFunds";
  private static final String ID_URL = RESOURCE_URL + "/{id}";
  private static final String SOURCE_OF_FUND_CODE_ONE = "FUNDCODE1";
  private static final String SOURCE_OF_FUND_NAME_ONE = "FUNDNAME1";
  private static final Integer DISPLAY_ORDER_ONE = 1;

  private static final String FORMAT_PARAM = "format";
  private static final String ZONE_SUMMARY_REPORT_URL = RESOURCE_URL
      + "/zoneSummaries/{" + FORMAT_PARAM + "}";
  private static final String FACILITY_SUMMARY_REPORT_URL = RESOURCE_URL
      + "/facilitySummaries/{" + FORMAT_PARAM + "}";
  
  private static final String PARAM_NAME_PROGRAM_ID = "programId";
  private static final String PARAM_NAME_FACILITY_ID = "facilityId";
  private static final String PARAM_NAME_PERIOD_ID = "periodId";
  private static final String PARAM_NAME_ZONE_ID = "zoneId";

  private UUID sourceOfFundId = UUID.randomUUID();
  private SourceOfFund sourceOfFund1 = new SourceOfFundDataBuilder()
      .withCode(SOURCE_OF_FUND_CODE_ONE)
      .withName(SOURCE_OF_FUND_NAME_ONE)
      .withDisplayOrder(DISPLAY_ORDER_ONE)
      .build();
  private SourceOfFundDto sourceOfFundDto = new SourceOfFundDto();

  @Before
  public void setUp() {
    sourceOfFund1.export(sourceOfFundDto);
    when(sourceOfFundRepository
        .saveAndFlush(any(SourceOfFund.class))).thenAnswer(new SaveAnswer<>());
    pageable = PageRequest.of(0, 10);
  }

  @Test
  public void postShouldCreateNewSourceOfFund() {

    SourceOfFundDto response = restAssured
        .given()
        .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
        .contentType(MediaType.APPLICATION_JSON_VALUE)
        .body(sourceOfFundDto)
        .when()
        .post(RESOURCE_URL)
        .then()
        .statusCode(201)
        .extract().as(SourceOfFundDto.class);

    assertEquals(SOURCE_OF_FUND_NAME_ONE, response.getName());
    assertEquals(SOURCE_OF_FUND_CODE_ONE, response.getCode());
    assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(), RamlMatchers.hasNoViolations());

  }

  @Test
  public void postShouldReturnBadRequestIfRequestedBodyIsInvalid() {
    sourceOfFundDto.setCode(null);

    restAssured.given()
        .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
        .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
        .body(sourceOfFundDto)
        .when()
        .post(RESOURCE_URL)
        .then()
        .statusCode(400)
        .body(MESSAGE_KEY, is(ERROR_CODE_OR_NAME_REQUIRED));

    // we don't check request body because of the purpose of the test
    assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(), RamlMatchers.validates());
    assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(), RamlMatchers.responseChecks());
  }

  @Test
  public void getSourceOfFundsShouldReturnUnauthorizedWithoutAuthorization() {

    restAssured.given()
        .pathParam(ID, sourceOfFundId.toString())
        .when()
        .get(ID_URL)
        .then()
        .statusCode(401);

    assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(),
        RamlMatchers.hasNoViolations());
  }

  @Test
  public void shouldReturnBadRequestIfRequestIsInvalidInPutSourceOfFund() {

    restAssured.given()
        .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
        .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
        .pathParam(ID, UUID.randomUUID())
        .body(sourceOfFundDto)
        .when()
        .put(ID_URL)
        .then()
        .statusCode(400)
        .body(MESSAGE_KEY, is(MessageKeys.ERROR_ID_MISMATCH));

    assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(), RamlMatchers.hasNoViolations());
  }

  @Test
  public void shouldUpdateSourceOfFund() {

    when(sourceOfFundRepository.findById(sourceOfFundId))
        .thenReturn(Optional.of(sourceOfFund1));
    when(sourceOfFundRepository
        .saveAndFlush(any(SourceOfFund.class))).thenAnswer(new SaveAnswer<>());

    SourceOfFundDto response = restAssured
        .given()
        .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
        .contentType(MediaType.APPLICATION_JSON_VALUE)
        .pathParam("id", sourceOfFundDto.getId())
        .body(sourceOfFundDto)
        .when()
        .put(ID_URL)
        .then()
        .statusCode(200)
        .extract().as(SourceOfFundDto.class);

    assertEquals(SOURCE_OF_FUND_NAME_ONE, response.getName());
    assertEquals(SOURCE_OF_FUND_CODE_ONE, response.getCode());
    assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(), RamlMatchers.hasNoViolations());
  }

  @Test
  public void putShouldCreateNewSourceOfFundForNonExistingSourceOfFund() {
    given(sourceOfFundRepository.findFirstByCode(SOURCE_OF_FUND_CODE_ONE))
        .willReturn(null);
    when(sourceOfFundRepository
        .saveAndFlush(any(SourceOfFund.class))).thenAnswer(new SaveAnswer<>());

    SourceOfFundDto response = restAssured
        .given()
        .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
        .contentType(MediaType.APPLICATION_JSON_VALUE)
        .pathParam("id", sourceOfFundDto.getId())
        .body(sourceOfFundDto)
        .when()
        .put(ID_URL)
        .then()
        .statusCode(200)
        .extract().as(SourceOfFundDto.class);

    assertEquals(SOURCE_OF_FUND_NAME_ONE, response.getName());
    assertEquals(SOURCE_OF_FUND_CODE_ONE, response.getCode());
    assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(), RamlMatchers.hasNoViolations());
  }

  @Test
  public void putShouldUpdateSourceOfFundForExistingSourceOfFund() {
    given(sourceOfFundRepository.findFirstByCode(SOURCE_OF_FUND_CODE_ONE))
        .willReturn(sourceOfFund1);
    given(sourceOfFundRepository.save(any()))
        .willReturn(sourceOfFund1);

    SourceOfFundDto response = restAssured
        .given()
        .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
        .contentType(MediaType.APPLICATION_JSON_VALUE)
        .pathParam("id", sourceOfFundDto.getId())
        .body(sourceOfFundDto)
        .when()
        .put(ID_URL)
        .then()
        .statusCode(200)
        .extract().as(SourceOfFundDto.class);

    assertEquals(SOURCE_OF_FUND_NAME_ONE, response.getName());
    assertEquals(SOURCE_OF_FUND_CODE_ONE, response.getCode());
    assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(), RamlMatchers.hasNoViolations());
  }

  @Test
  public void shouldReturnUnauthorizedIfTokenWasNotProvidedInPutSourceOfFund() {
    restAssured
        .given()
        .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
        .pathParam(ID, sourceOfFundDto.getId())
        .body(sourceOfFundDto)
        .when()
        .put(ID_URL)
        .then()
        .statusCode(401);

    assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(), RamlMatchers.hasNoViolations());
  }

  @Test
  public void getShouldGetSourceOfFund() {

    given(sourceOfFundRepository.findById(sourceOfFundId))
        .willReturn(Optional.of(sourceOfFund1));

    SourceOfFundDto response = restAssured
        .given()
        .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
        .pathParam("id", sourceOfFundId)
        .when()
        .get(ID_URL)
        .then()
        .statusCode(200)
        .extract().as(SourceOfFundDto.class);

    assertEquals(SOURCE_OF_FUND_NAME_ONE, response.getName());
    assertEquals(SOURCE_OF_FUND_CODE_ONE, response.getCode());
    assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(), RamlMatchers.hasNoViolations());
  }

  @Test
  public void shouldReturnPageOfSourceOfFunds() {
    given(sourceOfFundRepository.findAll(any(Pageable.class)))
        .willReturn(new PageImpl<>(Collections.singletonList(sourceOfFund1)));

    restAssured
        .given()
        .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
        .queryParam("page", pageable.getPageNumber())
        .queryParam("size", pageable.getPageSize())
        .when()
        .get(RESOURCE_URL)
        .then()
        .statusCode(HttpStatus.SC_OK)
        .body("content", hasSize(1))
        .body("content[0].id", is(sourceOfFund1.getId().toString()))
        .body("content[0].name", is(sourceOfFund1.getName()));

    assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(), RamlMatchers.hasNoViolations());
  }

  @Test
  public void getShouldReturnNotFoundForNonExistingSourceOfFund() {
    given(sourceOfFundRepository.findById(sourceOfFundId)).willReturn(Optional.empty());

    restAssured
        .given()
        .header(HttpHeaders.AUTHORIZATION, getClientTokenHeader())
        .pathParam("id", sourceOfFundId)
        .when()
        .get(ID_URL)
        .then()
        .statusCode(404)
        .body(MESSAGE_KEY, is(MessageKeys.ERROR_SOURCE_OF_FUND_NOT_FOUND));

    assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(), RamlMatchers.hasNoViolations());
  }

  private void assertResponseBody(ValidatableResponse response, String resourcePath,
                                  Matcher<String> idMatcher) {
    response
        .rootPath(resourcePath)
        .body(ID, idMatcher)
        .body("name", is(sourceOfFundDto.getName()))
        .body("code", is(sourceOfFundDto.getCode()));
  }

  @Test
  public void shouldGenerateFacilitySourceOfFundSummaryReportInHtmlFormat()
      throws JRException, ClassNotFoundException {
    testGenerateFacilitySourceOfFundSummaryReportInGivenFormat(REPORT_FORMAT_HTML);
  }

  @Test
  public void shouldGenerateFacilitySourceOfFundSummaryReportInPdfFormat()
      throws JRException, ClassNotFoundException {
    testGenerateFacilitySourceOfFundSummaryReportInGivenFormat(REPORT_FORMAT_PDF);
  }

  @Test
  public void shouldGenerateFacilitySourceOfFundSummaryReportInCsvFormat()
      throws JRException, ClassNotFoundException {
    testGenerateFacilitySourceOfFundSummaryReportInGivenFormat(REPORT_FORMAT_CSV);
  }

  @Test
  public void shouldGenerateFacilitySourceOfFundSummaryReportInXlsFormat()
      throws JRException, ClassNotFoundException {
    testGenerateFacilitySourceOfFundSummaryReportInGivenFormat(REPORT_FORMAT_XLS);
  }

  // Helper method
  private void testGenerateFacilitySourceOfFundSummaryReportInGivenFormat(String formatParam)
      throws JRException, ClassNotFoundException {
    given(sourceOfFundService.generateFacilityFundSourceSummaryReport(
        anyUuid(),
        anyUuid(),
        anyUuid(),
        eq(formatParam))
    ).willReturn(new byte[1]);

    restAssured.given()
        .header(HttpHeaders.AUTHORIZATION, getClientTokenHeader())
        .queryParam(PARAM_NAME_PROGRAM_ID, UUID.randomUUID().toString())
        .queryParam(PARAM_NAME_PERIOD_ID, UUID.randomUUID().toString())
        .queryParam(PARAM_NAME_FACILITY_ID, UUID.randomUUID().toString())
        .pathParam(FORMAT_PARAM, formatParam)
        .when()
        .get(FACILITY_SUMMARY_REPORT_URL)
        .then()
        .statusCode(HttpStatus.SC_OK);

    assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(), RamlMatchers.hasNoViolations());
  }

  @Test
  public void shouldGenerateZoneSourceOfFundSummaryReportInHtmlFormat()
      throws JRException, ClassNotFoundException {
    testGenerateZoneSourceOfFundSummaryReportInGivenFormat(REPORT_FORMAT_HTML);
  }

  @Test
  public void shouldGenerateZoneSourceOfFundSummaryReportInPdfFormat()
      throws JRException, ClassNotFoundException {
    testGenerateZoneSourceOfFundSummaryReportInGivenFormat(REPORT_FORMAT_PDF);
  }

  @Test
  public void shouldGenerateZoneSourceOfFundSummaryReportInCsvFormat()
      throws JRException, ClassNotFoundException {
    testGenerateZoneSourceOfFundSummaryReportInGivenFormat(REPORT_FORMAT_CSV);
  }

  @Test
  public void shouldGenerateZoneSourceOfFundSummaryReportInXlsFormat()
      throws JRException, ClassNotFoundException {
    testGenerateZoneSourceOfFundSummaryReportInGivenFormat(REPORT_FORMAT_XLS);
  }

  // Helper method
  private void testGenerateZoneSourceOfFundSummaryReportInGivenFormat(String formatParam)
      throws JRException, ClassNotFoundException {
    given(sourceOfFundService.generateZoneFundSourceSummaryReport(
        anyUuid(),
        anyUuid(),
        anyUuid(),
        eq(formatParam))
    ).willReturn(new byte[1]);

    restAssured.given()
        .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
        .queryParam(PARAM_NAME_PROGRAM_ID, UUID.randomUUID().toString())
        .queryParam(PARAM_NAME_PERIOD_ID, UUID.randomUUID().toString())
        .queryParam(PARAM_NAME_ZONE_ID, UUID.randomUUID().toString())
        .pathParam(FORMAT_PARAM, formatParam)
        .when()
        .get(ZONE_SUMMARY_REPORT_URL)
        .then()//then
        .statusCode(HttpStatus.SC_OK);

    assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(), RamlMatchers.hasNoViolations());
  }

  @Test
  public void shouldReturnUnAuthorizedWhenGenerateFacilitySourceOfFundSummaryUnAuthenticated() {

    restAssured.given()
        .queryParam(PARAM_NAME_PROGRAM_ID, UUID.randomUUID().toString())
        .queryParam(PARAM_NAME_PERIOD_ID, UUID.randomUUID().toString())
        .queryParam(PARAM_NAME_FACILITY_ID, UUID.randomUUID().toString())
        .pathParam(FORMAT_PARAM, REPORT_FORMAT_CSV)
        .when()
        .get(FACILITY_SUMMARY_REPORT_URL)
        .then()
        .statusCode(HttpStatus.SC_UNAUTHORIZED);

    assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(), RamlMatchers.hasNoViolations());
  }

  @Test
  public void shouldReturnBadRequestWhenGeneratingFacilitySourceFundSummaryForNonExistingFacility()
      throws JRException, ClassNotFoundException {
    given(sourceOfFundService.generateFacilityFundSourceSummaryReport(
        anyUuid(),
        anyUuid(),
        anyUuid(),
        eq(REPORT_FORMAT_CSV))
    ).willThrow(new ValidationMessageException("Test exception message"));

    restAssured.given()
        .header(HttpHeaders.AUTHORIZATION, getClientTokenHeader())
        .queryParam(PARAM_NAME_PROGRAM_ID, UUID.randomUUID().toString())
        .queryParam(PARAM_NAME_PERIOD_ID, UUID.randomUUID().toString())
        .queryParam(PARAM_NAME_FACILITY_ID, UUID.randomUUID().toString())
        .pathParam(FORMAT_PARAM, REPORT_FORMAT_CSV)
        .when()
        .get(FACILITY_SUMMARY_REPORT_URL)
        .then()
        .statusCode(HttpStatus.SC_BAD_REQUEST);

    assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(), RamlMatchers.hasNoViolations());
  }

  @Test
  public void shouldReturnUnAuthorizedWhenGenerateZoneSourceOfFundSummaryUnAuthenticated() {

    restAssured.given()
        .queryParam(PARAM_NAME_PROGRAM_ID, UUID.randomUUID().toString())
        .queryParam(PARAM_NAME_PERIOD_ID, UUID.randomUUID().toString())
        .queryParam(PARAM_NAME_ZONE_ID, UUID.randomUUID().toString())
        .pathParam(FORMAT_PARAM, REPORT_FORMAT_CSV)
        .when()
        .get(ZONE_SUMMARY_REPORT_URL)
        .then()
        .statusCode(HttpStatus.SC_UNAUTHORIZED);

    assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(), RamlMatchers.hasNoViolations());
  }

  @Test
  public void shouldReturnBadRequestWhenGeneratingZoneSourceOfFundSummaryForNonExistingZone()
      throws JRException, ClassNotFoundException {
    given(sourceOfFundService.generateZoneFundSourceSummaryReport(
        anyUuid(),
        anyUuid(),
        anyUuid(),
        eq(REPORT_FORMAT_CSV))
    ).willThrow(new ValidationMessageException("Test exception message"));

    restAssured.given()
        .header(HttpHeaders.AUTHORIZATION, getClientTokenHeader())
        .queryParam(PARAM_NAME_PROGRAM_ID, UUID.randomUUID().toString())
        .queryParam(PARAM_NAME_PERIOD_ID, UUID.randomUUID().toString())
        .queryParam(PARAM_NAME_ZONE_ID, UUID.randomUUID().toString())
        .pathParam(FORMAT_PARAM, REPORT_FORMAT_CSV)
        .when()
        .get(ZONE_SUMMARY_REPORT_URL)
        .then()
        .statusCode(HttpStatus.SC_BAD_REQUEST);

    assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(), RamlMatchers.hasNoViolations());
  }
}
