/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.budget.web;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.jayway.restassured.response.ValidatableResponse;
import guru.nidi.ramltester.junit.RamlMatchers;
import java.util.Collections;
import java.util.Optional;
import java.util.UUID;
import org.apache.http.HttpStatus;
import org.hamcrest.Matcher;
import org.javers.core.commit.CommitId;
import org.javers.core.commit.CommitMetadata;
import org.javers.core.diff.changetype.ValueChange;
import org.javers.core.metamodel.object.GlobalId;
import org.javers.core.metamodel.object.UnboundedValueObjectId;
import org.javers.repository.jql.JqlQuery;
import org.joda.time.LocalDateTime;
import org.junit.Before;
import org.junit.Test;
import org.openlmis.budget.SourceOfFundDataBuilder;
import org.openlmis.budget.domain.Budget;
import org.openlmis.budget.domain.BudgetDataBuilder;
import org.openlmis.budget.domain.SourceOfFund;
import org.openlmis.budget.dto.referencedata.FacilityDto;
import org.openlmis.budget.dto.referencedata.ProgramDto;
import org.openlmis.budget.i18n.MessageKeys;
import org.openlmis.budget.service.BudgetService;
import org.openlmis.budget.service.referencedata.FacilityReferenceDataService;
import org.openlmis.budget.service.referencedata.ProgramReferenceDataService;
import org.openlmis.budget.util.DtoGenerator;
import org.openlmis.budget.web.budget.BudgetDto;
import org.openlmis.budget.web.budget.SourceOfFundDto;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

@SuppressWarnings("PMD.TooManyMethods")
public class BudgetControllerIntegrationTest extends
        BaseWebIntegrationTest {

  @MockBean(name = "facilityReferenceDataService")
  FacilityReferenceDataService facilityReferenceDataService;

  @MockBean(name = "programReferenceDataService")
  ProgramReferenceDataService programReferenceDataService;

  private static final String RESOURCE_URL = "/api/budgets";

  private static final String ID_URL = RESOURCE_URL + "/{id}";

  private static final String AUDIT_LOG_URL = ID_URL + "/auditLog";

  private Budget budget = new BudgetDataBuilder().build();
  private BudgetDto budgetDto = BudgetDto.newInstance(budget);
  private SourceOfFund sourceOfFund1;
  private static final String FACILITY_CODE = "FCode";
  private static final String PROGRAM_CODE = "PCode";

  private static final String NAME = "name";

  private GlobalId globalId = new UnboundedValueObjectId(Budget.class.getSimpleName());
  private ValueChange change = new ValueChange(globalId, NAME, "name1", "name2");

  private CommitId commitId = new CommitId(1, 0);
  private CommitMetadata commitMetadata = new CommitMetadata(
            "admin", Maps.newHashMap(), LocalDateTime.now(), commitId);
  private SourceOfFundDto sourceOfFundDto = new SourceOfFundDto();

  @MockBean(name = "budgetService")
  private BudgetService budgetService;

  @Before
  public void setUp() {

    sourceOfFund1 = new SourceOfFundDataBuilder().buildAsNew();
    sourceOfFund1.export(sourceOfFundDto);

    when(sourceOfFundRepository
            .saveAndFlush(any(SourceOfFund.class))).thenAnswer(new SaveAnswer<>());
    budgetDto.setSourceOfFundCode(sourceOfFundDto.getCode());
    budgetDto.setFacilityCode(FACILITY_CODE);
    budgetDto.setProgramCode(PROGRAM_CODE);
    given(budgetRepository
            .saveAndFlush(any(Budget.class))).willAnswer(new SaveAnswer<>());
    change.bindToCommit(commitMetadata);
  }

  @Test
  public void postShouldCreateNewBudget() {
    //Given

    FacilityDto facility = mockFacility();
    budgetDto.setFacilityId(facility.getId());
    ProgramDto program = mockProgram();
    budgetDto.setProgramId(program.getId());
    budgetDto.setSourceOfFundId(sourceOfFundDto.getId());

    doReturn(budgetDto)
            .when(budgetService).doInitiate(budgetDto);

    BudgetDto response = restAssured
            .given()
            .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .body(budgetDto)
            .when()
            .post(RESOURCE_URL)
            .then()
            .statusCode(201)
            .extract().as(BudgetDto.class);

    assertEquals(FACILITY_CODE, response.getFacilityCode());
    assertEquals(PROGRAM_CODE, response.getProgramCode());
    assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(), RamlMatchers.hasNoViolations());

  }

  @Test
  public void postShouldReturnBadRequestIfRequestedBodyIsInvalid() {
    //Given
    FacilityDto facility = mockFacility();
    budgetDto.setFacilityId(facility.getId());
    ProgramDto program = mockProgram();
    budgetDto.setProgramId(program.getId());
    budgetDto.setSourceOfFundId(sourceOfFundDto.getId());
    //invalid facility code
    budgetDto.setFacilityCode(null);
    doReturn(budgetDto)
            .when(budgetService).doInitiate(budgetDto);

    restAssured.given()
            .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
            .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
            .body(budgetDto)
            .when()
            .post(RESOURCE_URL)
            .then()
            .statusCode(400)
            .body(MESSAGE_KEY, is(MessageKeys.MUST_CONTAIN_VALUE));

    // we don't check request body because of the purpose of the test
    assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(), RamlMatchers.validates());
    assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(), RamlMatchers.responseChecks());
  }

  @Test
  public void putShouldCreateNewBudgetForNonExistingBudget() {
    //Given
    FacilityDto facility = mockFacility();
    budgetDto.setFacilityId(facility.getId());
    ProgramDto program = mockProgram();
    budgetDto.setProgramId(program.getId());
    budgetDto.setSourceOfFundId(sourceOfFundDto.getId());

    UUID nonExistingId = UUID.randomUUID();

    doReturn(budgetDto)
            .when(budgetService).doInitiate(budgetDto);

    when(budgetRepository.findById(nonExistingId)).thenReturn(Optional.of(budget));

    BudgetDto response = restAssured
            .given()
            .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .pathParam("id", budgetDto.getId())
            .body(budgetDto)
            .when()
            .put(ID_URL)
            .then()
            .statusCode(200)
            .extract().as(BudgetDto.class);

    assertEquals(FACILITY_CODE, response.getFacilityCode());
    assertEquals(PROGRAM_CODE, response.getProgramCode());
    assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(), RamlMatchers.hasNoViolations());
  }

  @Test
  public void putShouldUpdateBudgetForExistingBudget() {
    //Given
    FacilityDto facility = mockFacility();
    budgetDto.setFacilityId(facility.getId());
    ProgramDto program = mockProgram();
    budgetDto.setProgramId(program.getId());
    budgetDto.setSourceOfFundId(sourceOfFundDto.getId());

    doReturn(budgetDto)
            .when(budgetService).doInitiate(budgetDto);

    when(budgetRepository.findById(budgetDto.getId())).thenReturn(Optional.of(budget));

    BudgetDto response = restAssured
            .given()
            .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .pathParam("id", budgetDto.getId())
            .body(budgetDto)
            .when()
            .put(ID_URL)
            .then()
            .statusCode(200)
            .extract().as(BudgetDto.class);

    assertEquals(FACILITY_CODE, response.getFacilityCode());
    assertEquals(PROGRAM_CODE, response.getProgramCode());
    assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(), RamlMatchers.hasNoViolations());
  }

  @Test
  public void shouldReturnBadRequestIfRequestIsInvalidInPutBudget() {

    restAssured.given()
            .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
            .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
            .pathParam(ID, UUID.randomUUID())
            .body(budgetDto)
            .when()
            .put(ID_URL)
            .then()
            .statusCode(400)
            .body(MESSAGE_KEY, is(MessageKeys.ERROR_ID_MISMATCH));

    assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(), RamlMatchers.hasNoViolations());
  }

  @Test
  public void shouldReturnUnauthorizedIfTokenWasNotProvidedInPutBudget() {
    restAssured
            .given()
            .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
            .pathParam(ID, budgetDto.getId())
            .body(budgetDto)
            .when()
            .put(ID_URL)
            .then()
            .statusCode(401);

    assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(), RamlMatchers.hasNoViolations());
  }

  @Test
  public void getShouldGetBudget() {

    given(budgetRepository.findById(budgetDto.getId()))
            .willReturn(Optional.of(budget));

    BudgetDto response = restAssured
            .given()
            .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
            .pathParam("id", budgetDto.getId().toString())
            .when()
            .get(ID_URL)
            .then()
            .statusCode(200)
            .extract().as(BudgetDto.class);

    assertEquals(FACILITY_CODE, response.getFacilityCode());
    assertEquals(PROGRAM_CODE, response.getProgramCode());
    assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(), RamlMatchers.hasNoViolations());
  }

  @Test
  public void getShouldReturnNotFoundForNonExistingBudget() {
    given(budgetRepository.findById(budgetDto.getId())).willReturn(Optional.empty());

    restAssured
            .given()
            .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
            .pathParam("id", budgetDto.getId())
            .when()
            .get(ID_URL)
            .then()
            .statusCode(404)
            .body(MESSAGE_KEY, is(MessageKeys.ERROR_BUDGET_NOT_FOUND));

    assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(), RamlMatchers.hasNoViolations());
  }

  @Test
  public void shouldReturnPageOfBudgets() {
    given(budgetRepository.findAll(any(Pageable.class)))
             .willReturn(new PageImpl<>(Collections.singletonList(budget)));

    restAssured
             .given()
             .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
             .queryParam("page", pageable.getPageNumber())
             .queryParam("size", pageable.getPageSize())
             .when()
             .get(RESOURCE_URL)
             .then()
             .statusCode(HttpStatus.SC_OK)
             .body("content", hasSize(1))
             .body("content[0].id", is(budget.getId().toString()))
             .body("content[0].sourceOfFundId", is(budget.getSourceOfFundId().toString()));

    assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(), RamlMatchers.hasNoViolations());
  }

  @Test
  public void shouldReturnUnauthorizedForAllBudgetsEndpointIfUserIsNotAuthorized() {
    restAssured.given()
            .when()
            .get(RESOURCE_URL)
            .then()
            .statusCode(401);

    assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(), RamlMatchers.hasNoViolations());
  }

  @Test
  public void shouldRetrieveAuditLogs() {
    given(budgetRepository.existsById(budgetDto.getId())).willReturn(true);
    willReturn(Lists.newArrayList(change)).given(javers).findChanges(any(JqlQuery.class));

    restAssured
            .given()
            .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
            .pathParam(ID, budgetDto.getId().toString())
            .when()
            .get(AUDIT_LOG_URL)
            .then()
            .statusCode(HttpStatus.SC_OK)
            .body("", hasSize(1))
            .body("changeType", hasItem(change.getClass().getSimpleName()))
            .body("globalId.valueObject", hasItem(Budget.class.getSimpleName()))
            .body("commitMetadata.author", hasItem(commitMetadata.getAuthor()))
            .body("commitMetadata.properties", hasItem(hasSize(0)))
            .body("commitMetadata.commitDate", hasItem(commitMetadata.getCommitDate().toString()))
            .body("commitMetadata.id", hasItem(commitId.valueAsNumber().floatValue()))
            .body("property", hasItem(change.getPropertyName()))
            .body("left", hasItem(change.getLeft().toString()))
            .body("right", hasItem(change.getRight().toString()));

    assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(), RamlMatchers.hasNoViolations());
  }

  @Test
  public void shouldRetrieveAuditLogsWithParameters() {
    given(budgetRepository.existsById(budgetDto.getId())).willReturn(true);
    willReturn(Lists.newArrayList(change)).given(javers).findChanges(any(JqlQuery.class));

    restAssured
            .given()
            .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
            .pathParam(ID, budgetDto.getId().toString())
            .queryParam("author", commitMetadata.getAuthor())
            .queryParam("changedPropertyName", change.getPropertyName())
            .when()
            .get(AUDIT_LOG_URL)
            .then()
            .statusCode(HttpStatus.SC_OK)
            .body("", hasSize(1))
            .body("changeType", hasItem(change.getClass().getSimpleName()))
            .body("globalId.valueObject", hasItem(Budget.class.getSimpleName()))
            .body("commitMetadata.author", hasItem(commitMetadata.getAuthor()))
            .body("commitMetadata.properties", hasItem(hasSize(0)))
            .body("commitMetadata.commitDate", hasItem(commitMetadata.getCommitDate().toString()))
            .body("commitMetadata.id", hasItem(commitId.valueAsNumber().floatValue()))
            .body("property", hasItem(change.getPropertyName()))
            .body("left", hasItem(change.getLeft().toString()))
            .body("right", hasItem(change.getRight().toString()));

    assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(), RamlMatchers.hasNoViolations());
  }

  @Test
  public void shouldReturnNotFoundMessageIfBudgetDoesNotExistForAuditLogEndpoint() {
    given(budgetRepository.existsById(budgetDto.getId())).willReturn(false);

    restAssured
            .given()
            .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
            .pathParam(ID, budgetDto.getId().toString())
            .when()
            .get(AUDIT_LOG_URL)
            .then()
            .statusCode(HttpStatus.SC_NOT_FOUND);

    assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(), RamlMatchers.hasNoViolations());
  }

  @Test
  public void shouldReturnUnauthorizedForAuditLogEndpointIfUserIsNotAuthorized() {
    restAssured
            .given()
            .pathParam(ID, budgetDto.getId().toString())
            .when()
            .get(AUDIT_LOG_URL)
            .then()
            .statusCode(401);

    assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(), RamlMatchers.hasNoViolations());
  }

  FacilityDto mockFacility() {
    FacilityDto facilityDto = DtoGenerator.of(FacilityDto.class);

    when(facilityReferenceDataService.findOne(anyUuid())).thenReturn(facilityDto);

    return facilityDto;
  }

  ProgramDto mockProgram() {
    ProgramDto programDto = DtoGenerator.of(ProgramDto.class);

    given(programReferenceDataService.findOne(anyUuid())).willReturn(programDto);

    return programDto;
  }

  private void assertResponseBody(ValidatableResponse response, String resourcePath,
                                  Matcher<String> idMatcher) {
    response
            .rootPath(resourcePath)
            .body(ID, idMatcher)
            .body("facilityId", is(budgetDto.getFacilityId()))
            .body("programId", is(budgetDto.getProgramId()))
            .body("sourceOfFundId", is(budgetDto.getSourceOfFundId()))
            .body("budgetAmount", is(budgetDto.getBudgetAmount()))
            .body("note", is(budgetDto.getNote()));
  }

}
