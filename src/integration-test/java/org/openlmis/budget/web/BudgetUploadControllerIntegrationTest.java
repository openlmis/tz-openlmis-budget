/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.budget.web;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.openlmis.budget.i18n.CsvUploadMessageKeys.ERROR_UPLOAD_RECORD_INVALID;

import com.jayway.restassured.response.Response;
import guru.nidi.ramltester.junit.RamlMatchers;
import java.io.IOException;
import java.util.Collections;
import org.junit.Test;
import org.mockito.Mockito;
import org.openlmis.budget.domain.SourceOfFund;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

/**
 * Controller used to expose Budget for Csv upload via HTTP.
 */

@SuppressWarnings({"PMD.TooManyMethods"})
public class BudgetUploadControllerIntegrationTest
        extends BaseWebIntegrationTest {

  private static final String RESOURCE_URL = "/api/budgets/upload";

  @Test
  public void shouldDownloadCsvWithHeadersOnly() throws IOException {
    Mockito.when(budgetRepository.findAll())
            .thenReturn(Collections.emptyList());

    String csvContent = download()
            .then()
            .statusCode(200)
            .extract().body().asString();

    verify(budgetRepository).findAll();
    assertEquals("Facility Code,Program Code,Source Of Fund Code,Note,"
            + "Budget Amount\r\n", csvContent);
    assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(), RamlMatchers.hasNoViolations());
  }

  @Test
  public void shouldNotUploadCsvWithInvalidColumnValues() throws IOException {
    // given
    ClassPathResource basicCsvToUpload =
            new ClassPathResource("csv/budgets/csvWithInvalidColumnValues.csv");

    // Error message based on invalid value in given csv file
    String errorMsg = getMessage(
            ERROR_UPLOAD_RECORD_INVALID, 1, "'' could not be parsed as facility code");

    // when
    upload(basicCsvToUpload)
            .then()
            .statusCode(400)
            .body(MESSAGE, equalTo(errorMsg));

    // then
    verify(sourceOfFundRepository, never()).save(any(SourceOfFund.class));
    // changed to responseChecks because file parameter is required
    // and RAML check does not recognizes it in request
    assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(), RamlMatchers.responseChecks());
  }

  private Response upload(ClassPathResource basicCsvToUpload) throws IOException {
    return restAssured.given()
            .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
            .contentType(MediaType.MULTIPART_FORM_DATA_VALUE)
            .queryParam("format", "csv")
            .multiPart(FILE_PARAM_NAME,
                    basicCsvToUpload.getFilename(),
                    basicCsvToUpload.getInputStream())
            .when()
            .post(RESOURCE_URL);
  }

  private Response download() {
    return restAssured.given()
            .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
            .contentType("text/csv")
            .queryParam("format", "csv")
            .when()
            .get(RESOURCE_URL);
  }
}
