/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.budget.repository;

import static org.junit.Assert.assertEquals;

import java.util.Optional;
import java.util.UUID;
import org.junit.Before;
import org.junit.Test;
import org.openlmis.budget.SourceOfFundDataBuilder;
import org.openlmis.budget.domain.RequisitionBudget;
import org.openlmis.budget.domain.RequisitionBudgetDataBuilder;
import org.openlmis.budget.domain.SourceOfFund;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

@SuppressWarnings({"PMD.TooManyMethods"})
public class RequisitionBudgetRepositoryIntegrationTest
        extends BaseCrudRepositoryIntegrationTest<RequisitionBudget> {

  @Autowired
  private RequisitionBudgetRepository requisitionBudgetRepository;

  @Autowired
  private SourceOfFundRepository sourceOfFundRepository;

  private SourceOfFund sourceOfFund
          = new SourceOfFundDataBuilder().buildAsNew();

  private RequisitionBudget requisitionBudget;

  private Pageable pageable = PageRequest.of(10, 1);

  @Override
  RequisitionBudget generateInstance() {

    return new RequisitionBudgetDataBuilder()
            .withSourceOfFundId(sourceOfFund.getId())
            .buildAsNew();
  }

  @Before
  public void setup() {
    sourceOfFundRepository.save(sourceOfFund);

    requisitionBudget = generateInstance();
    requisitionBudgetRepository.save(requisitionBudget);

  }

  @Override
  RequisitionBudgetRepository getRepository() {
    return this.requisitionBudgetRepository;
  }

  @Test
  public void shouldFindRequisitionBudgetWithRequisitionIdAndSourceOfFundId() {
    requisitionBudget.setSourceOfFundId(sourceOfFund.getId());
    requisitionBudget.setRequisitionId(UUID.randomUUID());
    requisitionBudgetRepository.save(requisitionBudget);

    Optional<RequisitionBudget> searchedRequisitionBudget
            = requisitionBudgetRepository
            .findByRequisitionIdAndSourceOfFundId(
                    requisitionBudget.getRequisitionId(),
                    requisitionBudget.getSourceOfFundId());
    assertEquals(searchedRequisitionBudget.isPresent(), true);
    assertEquals(searchedRequisitionBudget.get().getRequisitionId(),
            requisitionBudget.getRequisitionId());
    assertEquals(searchedRequisitionBudget.get().getSourceOfFundId(),
            requisitionBudget.getSourceOfFundId());

  }

  @Test
  public void shouldFindPageableRequisitionBudget() {
    // given
    requisitionBudget.setRequisitionId(UUID.randomUUID());
    requisitionBudgetRepository.save(requisitionBudget);

    //when
    Page<RequisitionBudget> requisitionBudgets =
            requisitionBudgetRepository.findByRequisitionId(
                    requisitionBudget.getRequisitionId(), pageable);
    // then
    assertEquals(requisitionBudgets.getContent().size(), 0);
  }

}
