/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.budget.repository;

import static junit.framework.TestCase.assertNull;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.math.BigDecimal;
import java.util.UUID;
import org.junit.Test;
import org.openlmis.budget.SourceOfFundDataBuilder;
import org.openlmis.budget.domain.Budget;
import org.openlmis.budget.domain.BudgetDataBuilder;
import org.openlmis.budget.domain.SourceOfFund;
import org.springframework.beans.factory.annotation.Autowired;

@SuppressWarnings({"PMD.TooManyMethods"})
public class BudgetRepositoryIntegrationTest
       extends BaseCrudRepositoryIntegrationTest<Budget> {

  private UUID facilityId = UUID.fromString("78d42bdb-9150-4c52-bd77-e42d777cfaed");
  private UUID programId = UUID.fromString("466b2e7f-5798-4027-a2ca-373627748ea6");

  @Autowired
  private BudgetRepository budgetRepository;

  @Autowired
  private SourceOfFundRepository sourceOfFundRepository;

  private SourceOfFund sourceOfFund;

  private Budget budget;

  @Override
  BudgetRepository getRepository() {

    return budgetRepository;

  }

  @Override
  Budget generateInstance() {
    sourceOfFund = sourceOfFundRepository
            .save(new SourceOfFundDataBuilder()
                          .withCode("CODE2")
                          .withName("NAME2")
                          .withDisplayOrder(2)
                          .buildAsNew());
    sourceOfFundRepository.save(sourceOfFund);
    return new BudgetDataBuilder()
            .withFacilityId(facilityId)
            .withProgramId(programId)
            .withBudgetAmount(new BigDecimal(1000))
            .withNote("some note")
            .withSourceOfFundId(sourceOfFund.getId())
            .buildAsNew();
  }

  public void setup() {
    budget = this.generateInstance();
    budgetRepository.save(budget);
  }

  @Test
  public void shouldNotGetBudgetByFacilityIdAndSourceOfFundIdIfDoesNotExists() {
    //given
    UUID fakeFacilityId = UUID.randomUUID();
    UUID fakeSourceOfFundId = UUID.randomUUID();

    //when
    Budget budget = budgetRepository
            .findByFacilityIdAndSourceOfFundId(
                    fakeFacilityId, fakeSourceOfFundId);
    //then
    assertNull(budget);
  }

  @Test
  public void shouldFindBudget() {
    setup();
    //given
    UUID facilityId = budget.getFacilityId();
    UUID sourceOfFundId = budget.getSourceOfFundId();

    //when
    Budget budget2 = budgetRepository
                    .findByFacilityIdAndSourceOfFundId(
                     facilityId, sourceOfFundId);
    //then
    assertNotNull(budget2);

  }

}
