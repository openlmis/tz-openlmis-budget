/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.budget.repository;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.hasItems;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.List;
import org.junit.Assert;
import org.junit.Test;
import org.openlmis.budget.SourceOfFundDataBuilder;
import org.openlmis.budget.domain.SourceOfFund;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

@SuppressWarnings({"PMD.TooManyMethods"})
public class SourceOfFundRepositoryIntegrationTest
      extends BaseCrudRepositoryIntegrationTest<SourceOfFund> {

  private SourceOfFund sourceOfFund1;
  private SourceOfFund sourceOfFund2;
  private PageRequest pageable = PageRequest.of(0, 2);

  @Autowired
  private SourceOfFundRepository sourceOfFundRepository;

  @Override
  SourceOfFundRepository getRepository() {
    return this.sourceOfFundRepository;
  }

  @Override
  SourceOfFund generateInstance() {
    return new SourceOfFundDataBuilder()
             .withName("name" + getNextInstanceNumber())
             .withCode("code" + getNextInstanceNumber())
             .buildAsNew();
  }

  @Test
  public void shouldCheckIfSourceOfFundExistsByCode() {
    //given
    sourceOfFund1 = generateInstance();
    sourceOfFundRepository.saveAndFlush(sourceOfFund1);

    //then
    assertNull(sourceOfFundRepository
            .findFirstByCode("some-random-code"));
    assertNotNull(sourceOfFundRepository
            .findFirstByCode(sourceOfFund1.getCode()));
  }

  @Test
  public void shouldAllowForSeveralSourceOfFunds() {

    sourceOfFund1 = new SourceOfFundDataBuilder()
            .withName("name" + getNextInstanceNumber())
            .withCode("code" + getNextInstanceNumber())
            .buildAsNew();
    sourceOfFund2 = new SourceOfFundDataBuilder()
            .withName("name" + getNextInstanceNumber())
            .withCode("code" + getNextInstanceNumber())
            .buildAsNew();

    long count = sourceOfFundRepository.count();

    sourceOfFundRepository.saveAndFlush(sourceOfFund1);
    sourceOfFundRepository.saveAndFlush(sourceOfFund2);

    assertThat(sourceOfFundRepository.count()).isEqualTo(count + 2);
  }

  @Test(expected = DataIntegrityViolationException.class)
  public void shouldNotAllowForSeveralSourceOfFundWithSameCode() {
    sourceOfFund1 = generateInstance();
    sourceOfFund2 = generateInstance();
    sourceOfFund2.setCode(sourceOfFund1.getCode());

    sourceOfFundRepository.saveAndFlush(sourceOfFund1);
    sourceOfFundRepository.saveAndFlush(sourceOfFund2);
  }

  @Test
  public void shouldFindSourceOfFundByCode() {
    sourceOfFund1 = new SourceOfFundDataBuilder()
            .withName("some-other-name")
            .withCode("some-code")
            .withDisplayOrder(5)
            .buildAsNew();
    sourceOfFundRepository.save(sourceOfFund1);

    List<SourceOfFund> result = sourceOfFundRepository
            .findByCodeIgnoreCase(sourceOfFund1.getCode());
    assertEquals(1, result.size());
  }

  @Test
  public void shouldReturnSourceOfFundListPage() {
    sourceOfFund1 = new SourceOfFundDataBuilder()
            .withName("some-other-name1")
            .withCode("some-code1")
            .withDisplayOrder(6)
            .buildAsNew();
    sourceOfFund2 = new SourceOfFundDataBuilder()
            .withName("some-other-name2")
            .withCode("some-code2")
            .withDisplayOrder(7)
            .buildAsNew();

    sourceOfFundRepository.save(sourceOfFund1);
    sourceOfFundRepository.save(sourceOfFund2);

    Page<SourceOfFund> sourceOfFunds = sourceOfFundRepository
            .findAllWithoutSnapshots(pageable);
    assertEquals(2, sourceOfFunds.getTotalElements());
    Assert.assertThat(sourceOfFunds.getContent(), hasItems(sourceOfFund1, sourceOfFund2));
  }

}
