/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.budget;

import java.util.Random;
import java.util.UUID;
import org.apache.commons.lang.RandomStringUtils;
import org.openlmis.budget.domain.SourceOfFund;

public class SourceOfFundDataBuilder {
  private static int instanceNumber = 0;

  private UUID id;
  private String name;
  private String code;
  private Integer displayOrder;

  /**
   * Builder for SourceOfFund entity instance.
   */
  public SourceOfFundDataBuilder() {
    instanceNumber++;

    this.id = UUID.fromString("10c5e0e7-2697-4146-8bb9-e34f57f1d156");
    this.name = "name" + instanceNumber;
    this.code = RandomStringUtils.randomAlphanumeric(10);
    this.displayOrder = new Random().nextInt();
  }

  public SourceOfFundDataBuilder withId(UUID id) {
    this.id = id;
    return this;
  }

  public SourceOfFundDataBuilder withName(String name) {
    this.name = name;
    return this;
  }

  public SourceOfFundDataBuilder withCode(String code) {
    this.code = code;
    return this;
  }

  public SourceOfFundDataBuilder withDisplayOrder(Integer displayOrder) {
    this.displayOrder = displayOrder;
    return this;
  }

  /**
   * Builds new instance of SourceOfFund (with id field).
   */
  public SourceOfFund build() {
    SourceOfFund sourceOfFund = buildAsNew();
    sourceOfFund.setId(id);

    return sourceOfFund;
  }

  /**
   * Builds new instance of SourceOfFund as a new object (without id field).
   */
  public SourceOfFund buildAsNew() {
    SourceOfFund sourceOfFund = new SourceOfFund();
    sourceOfFund.setName(name);
    sourceOfFund.setCode(code);
    sourceOfFund.setDisplayOrder(displayOrder);
    return sourceOfFund;
  }

}
