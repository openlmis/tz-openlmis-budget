/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.budget.domain;

import java.math.BigDecimal;
import java.util.UUID;

@SuppressWarnings({"PMD.TooManyMethods"})
public class RequisitionBudgetDataBuilder {

  private UUID id = UUID.fromString("35329395-7fe7-40cf-adc4-89a006746861");
  private UUID sourceOfFundId = UUID.fromString("78d42bdb-9150-4c52-bd77-e42d777cfaed");
  private UUID requisitionId = UUID.fromString("466b2e7f-5798-4027-a2ca-373627748ea6");
  private BigDecimal budgetAmount = BigDecimal.ZERO;

  public RequisitionBudgetDataBuilder withSourceOfFundId(UUID sourceOfFundId) {
    this.sourceOfFundId = sourceOfFundId;
    return this;
  }

  public RequisitionBudgetDataBuilder withRequisitionId(UUID requisitionId) {
    this.requisitionId = requisitionId;
    return this;
  }

  public RequisitionBudgetDataBuilder withBudgetAmount(BigDecimal budgetAmount) {
    this.budgetAmount = budgetAmount;
    return this;
  }

  /**
   * Builds new instance of requisition budget (with id field).
   */
  public RequisitionBudget build() {
    RequisitionBudget budget = buildAsNew();
    budget.setId(id);
    return budget;
  }

  /**
   * Builds new instance of RequisitionBudget as a new object (without id field).
  */
  public RequisitionBudget buildAsNew() {

    RequisitionBudget budget = new RequisitionBudget();

    budget.setRequisitionId(requisitionId);

    budget.setSourceOfFundId(sourceOfFundId);

    budget.setBudgetAmount(budgetAmount);

    return budget;

  }

}
