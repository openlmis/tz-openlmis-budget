/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.budget.domain;

import java.math.BigDecimal;
import java.util.UUID;

public class BudgetDataBuilder {
  private UUID id = UUID.fromString("35329395-7fe7-40cf-adc4-89a006746861");
  private UUID facilityId = UUID.fromString("78d42bdb-9150-4c52-bd77-e42d777cfaed");
  private UUID programId = UUID.fromString("466b2e7f-5798-4027-a2ca-373627748ea6");
  private UUID sourceOfFundId = UUID.fromString("10c5e0e7-2697-4146-8bb9-e34f57f1d156");
  private String facilityCode = "FCode";
  private String programCode = "PCode";
  private String sourceOfFundCode = "SCode";
  private BigDecimal budgetAmount = BigDecimal.ZERO;
  private String note = "The budget is sent from facility system";

  public BudgetDataBuilder withFacilityId(UUID facilityId) {
    this.facilityId = facilityId;
    return this;
  }

  public BudgetDataBuilder withProgramId(UUID programId) {
    this.programId = programId;
    return this;
  }

  public BudgetDataBuilder withSourceOfFundId(UUID sourceOfFundId) {
    this.sourceOfFundId = sourceOfFundId;
    return this;
  }

  public BudgetDataBuilder withBudgetAmount(BigDecimal budgetAmount) {
    this.budgetAmount = budgetAmount;
    return this;
  }

  public BudgetDataBuilder withNote(String note) {
    this.note = note;
    return this;
  }

  /**
     * Builds new instance of Budget (with id field).
  */
  public Budget build() {
    Budget budget = buildAsNew();
    budget.setId(id);

    return budget;
  }

  /**
     * Builds new instance of Budget as a new object (without id field).
  */
  public Budget buildAsNew() {

    Budget budget = new Budget();

    budget.setFacilityId(facilityId);

    budget.setProgramId(programId);

    budget.setSourceOfFundId(sourceOfFundId);

    budget.setBudgetAmount(budgetAmount);

    budget.setNote(note);

    budget.setProgramCode(programCode);

    budget.setSourceOfFundCode(sourceOfFundCode);

    budget.setFacilityCode(facilityCode);

    return budget;

  }

}
