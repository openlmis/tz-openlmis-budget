/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.budget.domain;

import static org.assertj.core.api.Assertions.assertThat;

import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;
import org.junit.Test;
import org.openlmis.budget.ToStringTestUtils;
import org.openlmis.budget.web.budget.BudgetDto;

public class BudgetTest {

  @Test
  public void equalsContract() {
    EqualsVerifier
            .forClass(Budget.class)
            .withRedefinedSuperclass()
            .suppress(Warning.NONFINAL_FIELDS)
            .verify();
  }

  @Test
  public void shouldImplementToString() {
    Budget budget = new BudgetDataBuilder().build();
    ToStringTestUtils.verify(Budget.class, budget, "TEXT", "INTEGER");
  }

  @Test
  public void shouldCreateNewInstance() {
    Budget budget = new BudgetDataBuilder().build();
    BudgetDto dto = BudgetDto.newInstance(budget);

    Budget newBudget = Budget.newInstance(dto);

    assertThat(newBudget).isEqualTo(budget);
  }

  @Test
  public void shouldUpdateFrom() {
    Budget budget = new BudgetDataBuilder().build();
    BudgetDto dto = BudgetDto.newInstance(budget);
    dto.setNote("msd");

    budget.updateFrom(dto);

    assertThat(budget.getNote()).isEqualTo("msd");
  }

  @Test
  public void shouldExportData() {
    Budget budget = new BudgetDataBuilder().build();
    BudgetDto dto = new BudgetDto();

    budget.export(dto);

    assertThat(dto.getId()).isEqualTo(budget.getId());
    assertThat(dto.getNote()).isEqualTo(budget.getNote());
  }

}
