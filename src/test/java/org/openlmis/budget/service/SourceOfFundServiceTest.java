/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.budget.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

import com.google.common.collect.Lists;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperPrint;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.openlmis.budget.SourceOfFundDataBuilder;
import org.openlmis.budget.domain.RequisitionBudget;
import org.openlmis.budget.domain.SourceOfFund;
import org.openlmis.budget.dto.referencedata.FacilityDto;
import org.openlmis.budget.dto.referencedata.GeographicZoneDto;
import org.openlmis.budget.dto.referencedata.MinimalFacilityDto;
import org.openlmis.budget.dto.requisition.BasicRequisitionDto;
import org.openlmis.budget.exception.ValidationMessageException;
import org.openlmis.budget.repository.RequisitionBudgetRepository;
import org.openlmis.budget.repository.SourceOfFundRepository;
import org.openlmis.budget.service.referencedata.FacilityReferenceDataService;
import org.openlmis.budget.service.referencedata.GeographicZoneDataService;
import org.openlmis.budget.util.DtoGenerator;
import org.openlmis.budget.util.FacilityDtoDataBuilder;
import org.openlmis.budget.util.GeographicZoneDtoDataBuilder;
import org.openlmis.budget.util.JasperReportHelper;
import org.openlmis.budget.util.RequisitionBudgetDataBuilder;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
@PrepareForTest(JasperReportHelper.class)
@SuppressWarnings("PMD.TooManyMethods")
public class SourceOfFundServiceTest {

  private static final String REPORT_FORMAT_CSV = "csv";
  private static final String REPORT_FORMAT_PDF = "pdf";
  private static final String REPORT_FORMAT_XLS = "xls";
  private static final String REPORT_FORMAT_HTML = "html";

  @Mock
  private FacilityReferenceDataService facilityReferenceDataService;
  @Mock
  private SourceOfFundRepository sourceOfFundRepository;
  @Mock
  private RequisitionBudgetRepository requisitionBudgetRepository;
  @Mock
  private RequisitionService requisitionService;
  @Mock
  private GeographicZoneDataService zoneService;

  @InjectMocks
  private SourceOfFundService sourceOfFundService;

  private SourceOfFund sourceOfFund1;
  private SourceOfFund sourceOfFund2;
  private List<SourceOfFund> sourceOfFunds;

  private FacilityDto facility1;
  private FacilityDto facility2;
  private List<MinimalFacilityDto> facilities;

  private BasicRequisitionDto requisition1;
  private BasicRequisitionDto requisition2;
  private List<BasicRequisitionDto> requisitions;

  private RequisitionBudget budget1;
  private RequisitionBudget budget2;

  private GeographicZoneDto zone1;

  private UUID programId;
  private UUID periodId;
  private UUID facilityId;
  private UUID zoneId;

  private byte[] expectedReport = new byte[1];

  @Before
  public void setUp() throws JRException {
    MockitoAnnotations.initMocks(this);

    programId = UUID.randomUUID();
    periodId = UUID.randomUUID();
    facilityId = UUID.randomUUID();
    zoneId = UUID.randomUUID();

    sourceOfFund1 = new SourceOfFundDataBuilder().build();
    sourceOfFund2 = new SourceOfFundDataBuilder().build();
    sourceOfFunds = Lists.newArrayList(sourceOfFund1, sourceOfFund2);

    when(sourceOfFundRepository.findAll())
        .thenReturn(sourceOfFunds);

    facility1 = new FacilityDtoDataBuilder().buildAsDto();
    facility2 = new FacilityDtoDataBuilder().buildAsDto();
    facility2 = DtoGenerator.of(FacilityDto.class);
    facilities = Lists.newArrayList(facility1, facility2);
    when(facilityReferenceDataService.findOne(any()))
        .thenReturn(facility1);
    when(facilityReferenceDataService.search(
        anyString(),
        anyString(),
        any(),
        anyBoolean()
    )).thenReturn(facilities);

    zone1 = new GeographicZoneDtoDataBuilder().buildAsDto();
    when(zoneService.findOne(any())).thenReturn(zone1);

    requisition1 = DtoGenerator.of(BasicRequisitionDto.class);
    requisition2 = DtoGenerator.of(BasicRequisitionDto.class);
    requisitions = Lists.newArrayList(requisition1, requisition2);
    when(requisitionService.search(any(RequestParameters.class))).thenReturn(requisitions);

    budget1 = new RequisitionBudgetDataBuilder().build();
    budget2 = new RequisitionBudgetDataBuilder().build();
    when(requisitionBudgetRepository.findByRequisitionIdAndSourceOfFundId(
        any(UUID.class),
        any(UUID.class)
    )).thenReturn(Optional.of(budget1))
        .thenReturn(Optional.of(budget2));

    PowerMockito.mockStatic(JasperReportHelper.class);

  }

  @Test
  public void shouldGenerateFacilitySourceOfFundSummaryReportAsPdf()
      throws JRException, ClassNotFoundException {
    when(JasperReportHelper.exportJasperReport(any(JasperPrint.class), eq(REPORT_FORMAT_PDF)))
        .thenReturn(expectedReport);

    byte[] report = sourceOfFundService.generateFacilityFundSourceSummaryReport(
        programId,
        periodId,
        facilityId,
        REPORT_FORMAT_PDF
    );

    assertEquals(expectedReport, report);
  }

  @Test
  public void shouldGenerateFacilitySourceOfFundSummaryReportAsCsv()
      throws JRException, ClassNotFoundException {
    when(JasperReportHelper.exportJasperReport(any(JasperPrint.class), eq(REPORT_FORMAT_CSV)))
        .thenReturn(expectedReport);

    byte[] report = sourceOfFundService.generateFacilityFundSourceSummaryReport(
        programId,
        periodId,
        facilityId,
        REPORT_FORMAT_CSV
    );

    assertEquals(expectedReport, report);
  }

  @Test
  public void shouldGenerateFacilitySourceOfFundSummaryReportAsXls()
      throws JRException, ClassNotFoundException {
    when(JasperReportHelper.exportJasperReport(any(JasperPrint.class), eq(REPORT_FORMAT_XLS)))
        .thenReturn(expectedReport);

    byte[] report = sourceOfFundService.generateFacilityFundSourceSummaryReport(
        programId,
        periodId,
        facilityId,
        REPORT_FORMAT_XLS
    );

    assertEquals(expectedReport, report);
  }

  @Test
  public void shouldGenerateFacilitySourceOfFundSummaryReportAsHtml()
      throws JRException, ClassNotFoundException {
    when(JasperReportHelper.exportJasperReport(any(JasperPrint.class), eq(REPORT_FORMAT_HTML)))
        .thenReturn(expectedReport);

    byte[] report = sourceOfFundService.generateFacilityFundSourceSummaryReport(
        programId,
        periodId,
        facilityId,
        REPORT_FORMAT_HTML
    );

    assertEquals(expectedReport, report);
  }


  @Test
  public void shouldGenerateZoneSourceOfFundSummaryReportAsPdf()
      throws JRException, ClassNotFoundException {
    when(JasperReportHelper.exportJasperReport(any(JasperPrint.class), eq(REPORT_FORMAT_PDF)))
        .thenReturn(expectedReport);

    byte[] report = sourceOfFundService.generateZoneFundSourceSummaryReport(
        programId,
        periodId,
        zoneId,
        REPORT_FORMAT_PDF
    );

    assertEquals(expectedReport, report);
  }

  @Test
  public void shouldGenerateZoneSourceOfFundSummaryReportAsCsv()
      throws JRException, ClassNotFoundException {
    when(JasperReportHelper.exportJasperReport(any(JasperPrint.class), eq(REPORT_FORMAT_CSV)))
        .thenReturn(expectedReport);

    byte[] report = sourceOfFundService.generateZoneFundSourceSummaryReport(
        programId,
        periodId,
        zoneId,
        REPORT_FORMAT_CSV
    );

    assertEquals(expectedReport, report);
  }

  @Test
  public void shouldGenerateZoneSourceOfFundSummaryReportAsXls()
      throws JRException, ClassNotFoundException {
    when(JasperReportHelper.exportJasperReport(any(JasperPrint.class), eq(REPORT_FORMAT_XLS)))
        .thenReturn(expectedReport);

    byte[] report = sourceOfFundService.generateZoneFundSourceSummaryReport(
        programId,
        periodId,
        zoneId,
        REPORT_FORMAT_XLS
    );

    assertEquals(expectedReport, report);
  }

  @Test
  public void shouldGenerateZoneSourceOfFundSummaryReportAsHtml()
      throws JRException, ClassNotFoundException {
    when(JasperReportHelper.exportJasperReport(any(JasperPrint.class), eq(REPORT_FORMAT_HTML)))
        .thenReturn(expectedReport);

    byte[] report = sourceOfFundService.generateZoneFundSourceSummaryReport(
        programId,
        periodId,
        zoneId,
        REPORT_FORMAT_HTML
    );

    assertEquals(expectedReport, report);
  }


  @Test(expected = ValidationMessageException.class)
  public void shouldFailWhenFacilityNotFoundOnGeneratingFacilitySourceOfFundSummary()
      throws JRException, ClassNotFoundException {
    when(facilityReferenceDataService.findOne(facilityId))
        .thenReturn(null);
    when(JasperReportHelper.exportJasperReport(any(JasperPrint.class), eq(REPORT_FORMAT_HTML)))
        .thenReturn(expectedReport);

    byte[] report = sourceOfFundService.generateFacilityFundSourceSummaryReport(
        programId,
        periodId,
        facilityId,
        REPORT_FORMAT_XLS
    );
  }

  @Test(expected = ValidationMessageException.class)
  public void shouldFailWhenZoneNotFoundOnGeneratingZoneSourceOfFundSummary()
      throws JRException, ClassNotFoundException {
    when(zoneService.findOne(zoneId)).thenReturn(null);
    when(JasperReportHelper.exportJasperReport(any(JasperPrint.class), eq(REPORT_FORMAT_XLS)))
        .thenReturn(expectedReport);

    byte[] report = sourceOfFundService.generateZoneFundSourceSummaryReport(
        programId,
        periodId,
        zoneId,
        REPORT_FORMAT_XLS
    );
  }

}
