/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.budget.util;

import java.math.BigDecimal;
import java.util.Random;
import java.util.UUID;
import org.openlmis.budget.domain.RequisitionBudget;

public class RequisitionBudgetDataBuilder {

  private UUID id;
  private UUID requisitionId;
  private UUID sourceOfFundId;
  private BigDecimal budgetAmount;

  /**
   * Builder for RequisitionBudget entity instances.
   */
  public RequisitionBudgetDataBuilder() {
    id = UUID.randomUUID();
    requisitionId = UUID.randomUUID();
    sourceOfFundId = UUID.randomUUID();
    budgetAmount = BigDecimal.valueOf(new Random().nextDouble());
  }

  public RequisitionBudgetDataBuilder withId(UUID id) {
    this.id = id;
    return this;
  }

  public RequisitionBudgetDataBuilder withRequisitionId(UUID requisitionId) {
    this.requisitionId = id;
    return this;
  }

  public RequisitionBudgetDataBuilder withSourceOfFundId(UUID sourceOfFundId) {
    this.sourceOfFundId = sourceOfFundId;
    return this;
  }

  public RequisitionBudgetDataBuilder withBudgetAmount(BigDecimal budgetAmount) {
    this.budgetAmount = budgetAmount;
    return this;
  }

  /**
   * Build an existing RequisitionBudget instance.
   * @return built instance of RequisitionBudget.
   */
  public RequisitionBudget build() {
    RequisitionBudget budget = new RequisitionBudget();
    budget.setId(this.id);
    budget.setSourceOfFundId(sourceOfFundId);
    budget.setRequisitionId(requisitionId);
    budget.setBudgetAmount(this.budgetAmount);
    return budget;
  }

  /**
   * Build an new RequisitionBudget instance with no Id.
   * @return built instance of RequisitionBudget.
   */
  public RequisitionBudget buildAsNew() {
    RequisitionBudget budget = build();
    budget.setId(null);
    return budget;
  }

}
