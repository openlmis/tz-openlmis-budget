--
-- Name: source_of_funds; Type: TABLE; Schema: budget; Owner: postgres; Tablespace:
-- Purpose: To store budget amount that was used to pay for the requisition.
--          There can be multiple amounts for a specific requisition differentiated by source of fund
--

CREATE TABLE source_of_funds (
	id uuid NOT NULL,
	code TEXT NOT NULL UNIQUE,
	name TEXT NOT NULL UNIQUE,
	displayorder integer,
	createddate timestamp with time zone,
	modifieddate timestamp with time zone
);

--
-- Name: source_of_funds_pkey; Type: CONSTRAINT; Schema: budget; Owner: postgres; Tablespace:
--

ALTER TABLE ONLY source_of_funds
    ADD CONSTRAINT source_of_funds_pkey PRIMARY KEY (id);
