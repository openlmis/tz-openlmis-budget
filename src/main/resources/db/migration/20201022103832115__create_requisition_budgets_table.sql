--
-- Name: requisition_budgets; Type: TABLE; Schema: budget; Owner: postgres; Tablespace:
-- Purpose: To store budget amount that was used to pay for the requisition.
--          There can be multiple amounts for a specific requisition differentiated by source of fund
--


CREATE TABLE requisition_budgets (
	id uuid NOT NULL,
	requisitionid uuid NOT NULL,
	sourceoffundid uuid NOT NULL,
	budgetamount numeric(19,2) NOT NULL,
	createddate timestamp with time zone DEFAULT NOW(),
	modifieddate timestamp with time zone DEFAULT NOW()
);

--
-- Name: requisition_budgets_pkey; Type: CONSTRAINT; Schema: budget; Owner: postgres; Tablespace:
--

ALTER TABLE ONLY requisition_budgets
    ADD CONSTRAINT requisition_budgets_pkey PRIMARY KEY (id);

ALTER TABLE requisition_budgets
  ADD CONSTRAINT fk_requisition_budget FOREIGN KEY (sourceoffundid) REFERENCES source_of_funds(id);