--
-- Name: budgets; Type: TABLE; Schema: budget; Owner: postgres; Tablespace:
-- Purpose: To store current budget for specific facility and program, by sources of fund.
--          This table is populated through API calls to source applications that maintains the budget
--          eLMIS does not maintain budget transactions. It always get the latest available balance from source application
--

CREATE TABLE budgets (
	id uuid NOT NULL,
	facilityid uuid NOT NULL,
	programid uuid NOT NULL,
	sourceoffundid uuid NOT NULL,
	lastupdateddate timestamp with time zone,
	budgetamount numeric(19,2) NOT NULL,
	note TEXT,
	createddate timestamp with time zone,
	modifieddate timestamp with time zone
);

--
-- Name: budgets_pkey; Type: CONSTRAINT; Schema: budgets; Owner: postgres; Tablespace:
--

ALTER TABLE ONLY budgets
    ADD CONSTRAINT budgets_pkey PRIMARY KEY (id);

ALTER TABLE budgets
    ADD CONSTRAINT fk_budgets_source_of_funds FOREIGN KEY (sourceoffundid) REFERENCES source_of_funds(id);
