/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.budget.service;

import ar.com.fdvs.dj.core.DynamicJasperHelper;
import ar.com.fdvs.dj.core.layout.ClassicLayoutManager;
import ar.com.fdvs.dj.domain.DynamicReport;
import ar.com.fdvs.dj.domain.builders.FastReportBuilder;
import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.openlmis.budget.domain.RequisitionBudget;
import org.openlmis.budget.domain.SourceOfFund;
import org.openlmis.budget.dto.referencedata.GeographicZoneDto;
import org.openlmis.budget.dto.referencedata.MinimalFacilityDto;
import org.openlmis.budget.dto.requisition.BasicRequisitionDto;
import org.openlmis.budget.exception.ValidationMessageException;
import org.openlmis.budget.i18n.MessageKeys;
import org.openlmis.budget.repository.RequisitionBudgetRepository;
import org.openlmis.budget.repository.SourceOfFundRepository;
import org.openlmis.budget.service.referencedata.FacilityReferenceDataService;
import org.openlmis.budget.service.referencedata.GeographicZoneDataService;
import org.openlmis.budget.util.JasperReportHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@SuppressWarnings("PMD.TooManyMethods")
public class SourceOfFundService {

  @Autowired
  private FacilityReferenceDataService facilityReferenceDataService;
  @Autowired
  private RequisitionService requisitionService;
  @Autowired
  private SourceOfFundRepository sourceOfFundRepository;
  @Autowired
  private RequisitionBudgetRepository requisitionBudgetRepository;
  @Autowired
  private GeographicZoneDataService zoneService;

  /**
   * Generate funds source summary report for the requisitions submitted in the specified zone.
   *
   * @param programId filter for program requisitions
   * @param periodId  filter for period requisitions
   * @param zoneId    filter for zone requisitions
   * @param format    the format for generating reports
   */
  public byte[] generateZoneFundSourceSummaryReport(
      UUID programId,
      UUID periodId,
      UUID zoneId,
      String format)
      throws ClassNotFoundException, JRException {
    if (zoneId == null) {
      throw new ValidationMessageException(MessageKeys.ERROR_ZONE_NOT_FOUND);
    }
    GeographicZoneDto zone = zoneService.findOne(zoneId);
    if (zone == null) {
      throw new ValidationMessageException(MessageKeys.ERROR_ZONE_NOT_FOUND);
    }


    FastReportBuilder drb = new FastReportBuilder();
    drb = drb.addColumn("Zone", "zone", String.class.getName(), 30);
    drb = drb.addColumn("Region", "region", String.class.getName(), 30);
    drb = drb.addColumn("District", "district", String.class.getName(), 30);
    drb = drb.addColumn("Facility Code", "facilityCode", String.class.getName(), 10);
    drb = drb.addColumn("Facility Name", "facilityName", String.class.getName(), 30);
    drb = drb.addColumn("Facility Type", "facilityType", String.class.getName(), 15);

    List<SourceOfFund> funds = sourceOfFundRepository.findAll();
    for (SourceOfFund fund : funds) {
      drb.addColumn(fund.getName(), fund.getName(), String.class.getName(), 20);
    }

    drb.setTitle("Zone Source of Funds Summary")
        .setSubtitle("Zone :" + zone.getName())
        .setPrintBackgroundOnOddRows(true)
        .setUseFullPageWidth(true);

    Map<String, Object> params = new LinkedHashMap<>();

    DynamicReport dr = drb.build();
    JasperReport jr = DynamicJasperHelper.generateJasperReport(
        dr,
        new ClassicLayoutManager(),
        params
    );

    List<MinimalFacilityDto> facilities = facilityReferenceDataService.search(
        null,
        null,
        zoneId,
        true);
    List<Map<String, Object>> summary = facilities.stream()
        .map(facility -> createFacilityFundSourceSummary(facility, funds, programId, periodId))
        .collect(Collectors.toList());
    JasperPrint jp = JasperFillManager.fillReport(
        jr,
        params,
        new JRBeanCollectionDataSource(summary)
    );
    return JasperReportHelper.exportJasperReport(jp, format);
  }

  private Map<String, Object> createFacilityFundSourceSummary(MinimalFacilityDto facility,
                                                              List<SourceOfFund> funds,
                                                              UUID programId,
                                                              UUID periodId) {
    Map<String, Object> summary = new LinkedHashMap<>();
    GeographicZoneDto district = facility.getGeographicZone();
    if (district != null) {
      summary.put("district", district.getName());
      GeographicZoneDto region = district.getParent();
      if (region != null) {
        summary.put("region", region.getName());
        GeographicZoneDto zone = region.getParent();
        if (zone != null) {
          summary.put("zone", zone.getName());
        }
      }
    }
    summary.put("facilityCode", facility.getCode());
    summary.put("facilityName", facility.getName());
    summary.put("facilityType", facility.getType().getName());


    for (SourceOfFund fund : funds) {
      BigDecimal budget = calculateTotalRequisitionBudget(
          fund,
          facility.getId(),
          programId,
          periodId
      );
      String budgetString = toMoneyString(budget);
      summary.put(fund.getName(), budgetString);
    }

    return summary;
  }

  private String toMoneyString(BigDecimal value) {
    return (value == null || BigDecimal.ZERO.compareTo(value) == 0)
        ? ""
        : NumberFormat.getInstance().format(value);
  }

  private BigDecimal calculateTotalRequisitionBudget(
      SourceOfFund fund,
      UUID facilityId,
      UUID programId,
      UUID periodId) {
    List<BasicRequisitionDto> requisitions = requisitionService.search(
        RequestParameters.init()
            .set("facility", facilityId)
            .set("program", programId)
            .set("processingPeriod", periodId)
    );

    BigDecimal totalBudget = requisitions.stream()
        .map(requisition -> requisitionBudgetRepository.findByRequisitionIdAndSourceOfFundId(
            requisition.getId(),
            fund.getId()
        ).map(RequisitionBudget::getBudgetAmount).orElse(BigDecimal.ZERO))
        .reduce(BigDecimal.ZERO, (total, budget) -> total.add(budget));

    return totalBudget;
  }

  /**
   * Generate funds source summary report for the requisitions submitted in the specified facility.
   *
   * @param programId  filter for program requisitions
   * @param periodId   filter for period requisitions
   * @param facilityId filter for facility requisitions
   * @param format     the format for generating reports
   */
  public byte[] generateFacilityFundSourceSummaryReport(
      UUID programId,
      UUID periodId,
      UUID facilityId,
      String format) throws JRException, ClassNotFoundException {
    if (facilityId == null) {
      throw new ValidationMessageException(MessageKeys.ERROR_FACILITY_NOT_FOUND);
    }
    MinimalFacilityDto facility = facilityReferenceDataService.findOne(facilityId);
    if (facility == null) {
      throw new ValidationMessageException(MessageKeys.ERROR_FACILITY_NOT_FOUND);
    }

    FastReportBuilder drb = new FastReportBuilder();

    drb = drb.addColumn("Fund Code", "fundCode", String.class.getName(), 30);
    drb = drb.addColumn("Fund Name", "fundName", String.class.getName(), 30);
    drb = drb.addColumn("Budget", "budget", String.class.getName(), 30);

    drb.setTitle("Facility Source of Funds Summary")
        .setSubtitle("Facility Name :" + facility.getName())
        .setPrintBackgroundOnOddRows(true)
        .setUseFullPageWidth(true);

    Map<String, Object> params = new LinkedHashMap<>();

    List<SourceOfFund> funds = sourceOfFundRepository.findAll();
    List<Map> data = funds.stream().map(fund -> {
      BigDecimal budget = calculateTotalRequisitionBudget(
          fund,
          facility.getId(),
          programId,
          periodId
      );
      String budgetString = toMoneyString(budget);

      Map<String, Object> summary = new LinkedHashMap<>();
      summary.put("fundCode", fund.getCode());
      summary.put("fundName", fund.getName());
      summary.put("budget", budgetString);

      return summary;
    }).collect(Collectors.toList());

    DynamicReport dr = drb.build();
    JasperReport jr = DynamicJasperHelper.generateJasperReport(
        dr,
        new ClassicLayoutManager(),
        params
    );
    JasperPrint jp = JasperFillManager.fillReport(
        jr,
        params,
        new JRBeanCollectionDataSource(data)
    );
    return JasperReportHelper.exportJasperReport(jp, format);
  }
}
