/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.budget.service;

import static org.openlmis.budget.util.RequestHelper.createUri;

import java.text.MessageFormat;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.openlmis.budget.dto.requisition.BasicRequisitionDto;
import org.openlmis.budget.dto.requisition.RequisitionDto;
import org.openlmis.budget.util.DynamicPageTypeReference;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

@Service
public class RequisitionService extends BaseCommunicationService<RequisitionDto> {

  public static final String ACCESS_TOKEN = "access_token";
  private final RestTemplate restTemplate = new RestTemplate();

  @Value("${requisition.url}")
  private String requisitionUrl;

  /**
   * Finds requisitions matching all of the provided parameters.
   */
  public List<BasicRequisitionDto> search(RequestParameters params) {
    String seachApiUrl = MessageFormat.format("{0}{1}search", getServiceUrl(), getUrl());

    RequestParameters parameters = RequestParameters.init()
        .setAll(params)
        .set(ACCESS_TOKEN, obtainUserAccessToken());

    ParameterizedTypeReference<PageDto<BasicRequisitionDto>> parameterizedType =
        new DynamicPageTypeReference<>(BasicRequisitionDto.class);

    PageDto<BasicRequisitionDto> page = restTemplate
        .exchange(createUri(seachApiUrl, parameters),
            HttpMethod.GET,
            null,
            parameterizedType)
        .getBody();

    return page.toList();
  }

  /**
   * Retrieves access token from the current HTTP context.
   *
   * @return token.
   */
  public String obtainUserAccessToken() {
    HttpServletRequest request = getCurrentHttpRequest();
    if (request == null) {
      return null;
    }

    String accessToken = request.getParameter(ACCESS_TOKEN);
    return accessToken;
  }

  private HttpServletRequest getCurrentHttpRequest() {
    RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
    if (requestAttributes instanceof ServletRequestAttributes) {
      HttpServletRequest request = ((ServletRequestAttributes) requestAttributes).getRequest();
      return request;
    }
    return null;
  }

  @Override
  protected String getServiceName() {
    return "Requisition";
  }

  @Override
  protected String getServiceUrl() {
    return requisitionUrl;
  }

  @Override
  protected String getUrl() {
    return "/api/requisitions/";
  }

  @Override
  protected Class<RequisitionDto> getResultClass() {
    return RequisitionDto.class;
  }

  @Override
  protected Class<RequisitionDto[]> getArrayResultClass() {
    return RequisitionDto[].class;
  }
}
