/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.budget.service;

import static org.openlmis.budget.i18n.MessageKeys.ERROR_MISSING_PARAMETER;
import static org.openlmis.budget.web.BaseController.MSD_CODE;
import static org.openlmis.budget.web.BaseController.PROGRAM_CODE;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import org.openlmis.budget.domain.Budget;
import org.openlmis.budget.domain.SourceOfFund;
import org.openlmis.budget.dto.referencedata.MinimalFacilityDto;
import org.openlmis.budget.dto.referencedata.ProgramDto;
import org.openlmis.budget.exception.ValidationMessageException;
import org.openlmis.budget.repository.BudgetRepository;
import org.openlmis.budget.repository.SourceOfFundRepository;
import org.openlmis.budget.service.referencedata.FacilityReferenceDataService;
import org.openlmis.budget.service.referencedata.ProgramReferenceDataService;
import org.openlmis.budget.util.Message;
import org.openlmis.budget.web.budget.BudgetDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Service;

@Service
@EnableAsync
@SuppressWarnings("PMD.TooManyMethods")
public class BudgetService {

  private static final Logger LOGGER = LoggerFactory.getLogger(BudgetService.class);

  @Autowired
  private SourceOfFundRepository sourceOfFundRepository;

  @Autowired
  private ProgramReferenceDataService programReferenceDataService;

  @Autowired
  private FacilityReferenceDataService facilityReferenceDataService;

  @Autowired
  private BudgetRepository budgetRepository;

  /**
   * This method initiates facilityId, programId and sourceOfFundId for given lists.
   *
   * @param facility     list of facility code.
   * @param program      list of program code.
   * @param sourceOfFund list of source of fund code.
   * @return BudgetDtos with similar codes.
   */
  public BudgetDto initiate(MinimalFacilityDto facility, ProgramDto program,
                            List<SourceOfFund> sourceOfFund, BudgetDto budget) {
    budget.setFacilityId(facility.getId());
    budget.setProgramId(program.getId());
    budget.setSourceOfFundId(sourceOfFund.get(0).getId());
    return budget;
  }

  /**
   * This method initiates facilityId, programId and sourceOfFundId for given lists.
   *
   * @param budgetDto provide budget dto data.
   * @return BudgetDtos with facilityId, programId and source of fund id.
   */
  public BudgetDto doInitiate(BudgetDto budgetDto) {
    if (null == budgetDto.getFacilityCode() && null == budgetDto.getProgramCode()
        && null == budgetDto.getSourceOfFundCode()) {
      throw new ValidationMessageException(
          new Message(ERROR_MISSING_PARAMETER));
    }

    MinimalFacilityDto facilityList =
        searchFacilityByCode(budgetDto.getFacilityCode());
    ProgramDto program = searchByProgramCode(budgetDto.getProgramCode());
    List<SourceOfFund> sourceOfFundList = sourceOfFundRepository
        .findByCodeIgnoreCase(budgetDto.getSourceOfFundCode());

    try {

      if (facilityList == null || program == null || sourceOfFundList.isEmpty()) {
        LOGGER.error("Facility, program, or source of fund is missing facility code: "
            + budgetDto.getFacilityCode());
        return null;
      }

      return initiate(facilityList, program, sourceOfFundList, budgetDto);
    } catch (Exception e) {
      LOGGER.error("Error while initiating budget", e);
      return null;
    }

  }

  private MinimalFacilityDto searchFacilityByCode(String facilityCode) {

    List<MinimalFacilityDto> searchData =
        facilityReferenceDataService.searchMinimal();

    return searchData.stream().filter(p -> p.getCode()
        .equals(facilityCode)).findAny().orElse(null);

  }

  private List<ProgramDto> searchProgramByCode(String programCode) {
    return programReferenceDataService.search(programCode);
  }

  private ProgramDto searchByProgramCode(String programCode) {
    return programReferenceDataService.searchByCode(programCode);
  }

  /**
   * Allows async to start processing in background .
   */
  @Async
  public void processBudgets(List<BudgetDto> budgetDtos) {
    for (BudgetDto budgetDto : budgetDtos) {
      if (budgetDto.getFacilityCode() == null) {
        LOGGER.error("BudgetDto must contain facilityCode");
        continue; // Skip this budgetDto and move to the next one
      }

      LOGGER.debug("Initiate and assign Ids from codes");

      budgetDto.setSourceOfFundCode(MSD_CODE);
      budgetDto.setProgramCode(PROGRAM_CODE);
      BudgetDto newBudgetDto = this.doInitiate(budgetDto);

      LOGGER.debug("Search available budget by facility id and source of fund id");

      if (newBudgetDto == null || newBudgetDto.getFacilityId() == null
          || newBudgetDto.getSourceOfFundId() == null) {
        LOGGER.error("Initiated BudgetDto must contain "
            + "FacilityId and SourceOfFundId");
        continue;
      }

      Budget db = budgetRepository
          .findByFacilityIdAndSourceOfFundId(newBudgetDto.getFacilityId(),
              newBudgetDto.getSourceOfFundId());

      if (db == null) {
        LOGGER.debug("Creating new budget");
        Budget newBudget = Budget.newInstance(newBudgetDto);
        newBudget.setId(null);
        budgetRepository.saveAndFlush(newBudget);
      } else {
        LOGGER.debug("Updating budget");
        budgetDto.setId(db.getId());
        db.updateFrom(newBudgetDto);
        budgetRepository.saveAndFlush(db);
      }
    }
  }

  /**
   * Parse budget dto.
   */
  public List<BudgetDto> parseBudgetDtos(String budgetDtosString) {
    Gson gson = new Gson();
    try {
      Type budgetDtoListType = new TypeToken<List<BudgetDto>>() {
      }.getType();
      return gson.fromJson(budgetDtosString, budgetDtoListType);
    } catch (JsonSyntaxException e) {
      LOGGER.error("Failed to parse budgetDtosString", e);
      return new ArrayList<>();
    }
  }

}
