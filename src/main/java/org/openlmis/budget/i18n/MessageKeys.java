/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.budget.i18n;

import java.util.Arrays;

public abstract class MessageKeys {
  private static final String DELIMITER = ".";

  private static final String SERVICE_PREFIX = "budget";
  private static final String ERROR = "error";
  static final String SERVICE_ERROR_PREFIX = join(SERVICE_PREFIX, "error");

  protected static final String SERVICE_ERROR = join(SERVICE_PREFIX, "error");
  private static final String SOURCEOFFUND = "sourceOfFund";
  private static final String SOURCE_OF_FUND = "sourceOfFund";
  private static final String JAVERS = "javers";
  protected static final String VALIDATION = "validation";
  protected static final String NULL = "null";
  public static final String ERROR_NULL = join(SERVICE_ERROR, NULL);
  private static final String ID = "id";
  private static final String CODE = "code";
  private static final String NAME = "name";
  private static final String ERROR_MISS_MATCH = join(SERVICE_ERROR, SOURCE_OF_FUND);
  protected static final String ID_MISMATCH = "idMismatch";

  private static final String MISMATCH = "mismatch";
  private static final String NOT_FOUND = "notFound";
  protected static final String REQUIRED = "required";
  protected static final String FIELD_ERROR = join(SOURCEOFFUND, CODE);

  public static final String ERROR_CODE_REQUIRED = "budget.error.sourceOfFund.code.required";
  private static final String PERMISSION = "permission";
  private static final String PERMISSIONS = PERMISSION + "s";
  private static final String MISSING = "missing";
  private static final String ORDER_FILE_TEMPLATE = "fileTemplate";
  private static final String CREATION = "creation";
  private static final String BUDGET = "budget";
  private static final String REQUISITION_BUDGET = "requisitionBudget";
  private static final String OBJECT_NULL = "objectNull";

  private static final String ERROR_PREFIX = join(SERVICE_PREFIX, ERROR);
  public static final String ERROR_SOURCE_OF_FUND_NOT_FOUND = join(ERROR_PREFIX,
          SOURCEOFFUND, NOT_FOUND);
  public static final String ERROR_CODE_OR_NAME_REQUIRED
          = join(ERROR_PREFIX, SOURCEOFFUND, CODE, NAME, REQUIRED);
  private static final String VALIDATION_ERROR = join(SERVICE_PREFIX, "validationError");
  public static final String MUST_CONTAIN_VALUE =
          join(VALIDATION_ERROR, "mustContainValue");
  public static final String MUST_BE_GREATER_THAN_OR_EQUAL_TO_ZERO =
          join(VALIDATION_ERROR, "mustBeGreaterThanOrEqualToZero");

  public static final String ERROR_JAVERS_EXISTING_ENTRY =
      join(ERROR_PREFIX, JAVERS, "entryAlreadyExists");

  public static final String PERMISSION_MISSING = join(ERROR_PREFIX, PERMISSION, MISSING);
  public static final String PERMISSIONS_MISSING = join(ERROR_PREFIX, PERMISSIONS, MISSING);

  public static final String ERROR_FILE_TEMPLATE_CREATION =
          join(ERROR_PREFIX, ORDER_FILE_TEMPLATE, CREATION);

  public static final String ERROR_REQUISITION_BUDGET_NOT_FOUND = join(ERROR_PREFIX,
          REQUISITION_BUDGET, NOT_FOUND);
  public static final String ERROR_BUDGET_NOT_FOUND = join(ERROR_PREFIX,
          BUDGET, NOT_FOUND);
  public static final String ERROR_FACILITY_NOT_FOUND = ERROR_PREFIX
          + ".facilityNotFound";
  public static final String ERROR_ZONE_NOT_FOUND = ERROR_PREFIX
      + ".zoneNotFound";
  public static final String ERROR_MISSING_PARAMETER = ERROR_PREFIX
          + ".missingParameter";

  public static final String ERROR_BUDGET_ID_MISMATCH = join(ERROR_PREFIX, BUDGET, ID, MISMATCH);
  public static final String ERROR_SOURCE_OF_FUND_ID_MISMATCH
          = join(ERROR_PREFIX, SOURCE_OF_FUND, ID, MISMATCH);
  public static final String ERROR_BUDGET_OBJECT_NULL = join(ERROR_PREFIX, BUDGET, OBJECT_NULL);
  public static final String ERROR_SERVICE_REQUIRED = ERROR_PREFIX + ".service.required";
  public static final String ERROR_SERVICE_OCCURED = ERROR_PREFIX + ".service.errorOccured";
  public static final String ERROR_IO = ERROR_PREFIX + ".io";
  public static final String ERROR_FORMAT_NOT_ALLOWED
          = ERROR_PREFIX + ".format.not.allowed";
  public static final String ERROR_FROM_FIELD_REQUIRED =
          join(ERROR_PREFIX, "field", REQUIRED);

  private static final String CONTEXTUAL_STATE = "contextualState";
  public static final String ERROR_FILE_IS_EMPTY = ERROR_PREFIX + ".file.empty";
  public static final String ERROR_INCORRECT_FILE_FORMAT = ERROR_PREFIX + ".incorrect.format";
  public static final String ERROR_ID_MISMATCH = join(ERROR_MISS_MATCH, ID_MISMATCH);

  public static final String ERROR_CONTEXTUAL_STATE_NULL =
      join(SERVICE_ERROR, CONTEXTUAL_STATE, NULL);

  private MessageKeys() {
    throw new UnsupportedOperationException();
  }

  private static String join(String... params) {
    return String.join(DELIMITER, Arrays.asList(params));
  }
}
