/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.budget.web.budget;

import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.stream.Collectors;
import org.openlmis.budget.domain.Budget;
import org.openlmis.budget.domain.SourceOfFund;
import org.openlmis.budget.exception.NotFoundException;
import org.openlmis.budget.exception.ValidationMessageException;
import org.openlmis.budget.i18n.MessageKeys;
import org.openlmis.budget.repository.BudgetRepository;
import org.openlmis.budget.repository.SourceOfFundRepository;
import org.openlmis.budget.service.BudgetService;
import org.openlmis.budget.util.Pagination;
import org.openlmis.budget.web.BaseController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.ext.XLogger;
import org.slf4j.ext.XLoggerFactory;
import org.slf4j.profiler.Profiler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Controller used to expose Budget via HTTP.
 */
@Controller
@RequestMapping(BudgetController.RESOURCE_PATH)
@Transactional
public class BudgetController extends BaseController {

  public static final String RESOURCE_PATH = API_PATH + "/budgets";

  private static final Logger LOGGER = LoggerFactory.getLogger(BudgetController.class);
  private final XLogger extLogger = XLoggerFactory.getXLogger(getClass());

  @Autowired
  private BudgetRepository budgetRepository;

  @Autowired
  private BudgetService budgetService;

  @Autowired
  private SourceOfFundRepository sourceOfFundRepository;

  /**
   * Allows the creation or updating of a budget. If the id is specified, it will be ignored.
   */
  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  @ResponseBody
  public BudgetDto createBudget(@RequestBody BudgetDto budgetDto) {
    if (Objects.isNull(budgetDto) || budgetDto.getFacilityCode() == null
        || budgetDto.getSourceOfFundCode() == null) {
      throw new ValidationMessageException(MessageKeys.MUST_CONTAIN_VALUE);
    }

    LOGGER.debug("Initiate and assign Ids from codes");

    BudgetDto newBudgetDto = budgetService.doInitiate(budgetDto);

    LOGGER.debug("Search available budget by facility id and source of fund id");

    Budget db = budgetRepository.findByFacilityIdAndSourceOfFundId(newBudgetDto.getFacilityId(),
        newBudgetDto.getSourceOfFundId());

    if (db == null) {
      LOGGER.debug("Creating new budget");
      Budget newBudget = Budget.newInstance(newBudgetDto);
      newBudget.setId(null);
      newBudget = budgetRepository.saveAndFlush(newBudget);

      return BudgetDto.newInstance(newBudget);
    } else {
      LOGGER.debug("Updating budget");
      budgetDto.setId(db.getId());

      db.updateFrom(newBudgetDto);

      budgetRepository.saveAndFlush(db);

      return BudgetDto.newInstance(db);
    }
  }

  /**
   * Allows the creation or updating of a budget. If the id is specified, it will be ignored.
   */
  @PostMapping(value = "/customerStatement")
  @ResponseStatus(HttpStatus.CREATED)
  @ResponseBody
  public String createStatements(@RequestBody String budgetDtosString) {
    // Parse the string input into List<BudgetDto>
    List<BudgetDto> budgetDtos = budgetService.parseBudgetDtos(budgetDtosString);
    if (budgetDtos == null || budgetDtos.isEmpty()) {
      throw new ValidationMessageException(MessageKeys.MUST_CONTAIN_VALUE);
    }

    // Process the budgets asynchronously
    budgetService.processBudgets(budgetDtos);

    // Return a response immediately
    return "Customer statement processing started";
  }

  /**
   * Updates the specified budget.
   */
  @PutMapping(value = "/{id}")
  @ResponseStatus(HttpStatus.OK)
  @ResponseBody
  public BudgetDto saveBudget(@PathVariable("id") UUID budgetId,
                              @RequestBody BudgetDto budgetDto) {
    if (null != budgetDto.getId() && !Objects.equals(budgetDto.getId(),
        budgetId)) {
      throw new ValidationMessageException(MessageKeys.ERROR_ID_MISMATCH);
    }

    Budget newBudget =
        Budget.newInstance(budgetDto);
    Budget budgetToUpdate = budgetRepository
        .findById(budgetId)
        .orElse(null);

    Budget budgetToSave;
    if (budgetToUpdate == null) {
      budgetToSave = newBudget;
      budgetToSave.setId(budgetId);
    } else {
      budgetToSave = budgetToUpdate;
      budgetToSave.updateFrom(budgetDto);
    }

    LOGGER.debug("Updating Budget");
    budgetToSave = budgetRepository
        .saveAndFlush(budgetToSave);
    return BudgetDto.newInstance(budgetToSave);
  }

  /**
   * Retrieves all budgets. Note that an empty collection rather than a 404 should be
   * returned if no budgets exist.
   */
  @GetMapping
  @ResponseStatus(HttpStatus.OK)
  @ResponseBody
  public Page<BudgetDto> getAllBudgets(Pageable pageable) {
    Page<Budget> page = budgetRepository.findAll(pageable);
    List<BudgetDto> content = page
        .getContent()
        .stream()
        .map(BudgetDto::newInstance)
        .collect(Collectors.toList());
    return Pagination.getPage(content, pageable, page.getTotalElements());
  }

  /**
   * Retrieves the specified budget.
   */
  @GetMapping(value = "/{id}")
  @ResponseStatus(HttpStatus.OK)
  @ResponseBody
  public BudgetDto getSpecifiedBudget(@PathVariable("id") UUID id) {
    Budget budget = budgetRepository.findById(id)
        .orElseThrow(() -> new NotFoundException(MessageKeys.ERROR_BUDGET_NOT_FOUND));

    return BudgetDto.newInstance(budget);
  }

  /**
   * Retrieves audit information related to the specified budget.
   *
   * @param author              The author of the changes which should be returned.
   *                            If null or empty, changes are returned regardless of author.
   * @param changedPropertyName The name of the property about which changes should be returned.
   *                            If null or empty, changes associated with any
   *                            and all properties are returned.
   * @param page                A Pageable object that allows client to optionally
   *                            add "page" (page number)
   *                            and "size" (page size) query parameters to the request.
   */
  @GetMapping(value = "/{id}/auditLog")
  @ResponseStatus(HttpStatus.OK)
  @ResponseBody
  public ResponseEntity<String> getBudgetAuditLog(@PathVariable("id") UUID id,
                                                  @RequestParam(name = "author",
                                                      required = false,
                                                      defaultValue = "") String author,
                                                  @RequestParam(name = "changedPropertyName",
                                                      required = false, defaultValue = "")
                                                  String changedPropertyName, Pageable page) {

    //Return a 404 if the specified instance can't be found
    if (!budgetRepository.existsById(id)) {
      throw new NotFoundException(MessageKeys.ERROR_BUDGET_NOT_FOUND);
    }

    return getAuditLogResponse(Budget.class, id, author, changedPropertyName, page);
  }

  /**
   * Retrieves Budgets by Facility.
   *
   * @param facilityId Facility id used to filter data
   * @return List of wanted budgets for a specified Facility
   */
  @GetMapping(value = "/{facilityId}/budgets")
  @ResponseStatus(HttpStatus.OK)
  @ResponseBody
  public List<BudgetDto> findBudgetByFacilityId(
      @PathVariable("facilityId") UUID facilityId
  ) {

    List<Budget> budgetList =
        budgetRepository.findByFacilityId(facilityId);

    return budgetList.stream().map(budget -> {
      BudgetDto budgetDto = BudgetDto.newInstance(budget);
      SourceOfFund sourceOfFund = sourceOfFundRepository
          .findById(budget.getSourceOfFundId()).get();
      budgetDto.setSourceOfFundName(sourceOfFund.getName());
      return budgetDto;
    }).collect(Collectors.toList());
  }

  Profiler getProfiler(String name, Object... entryArgs) {
    extLogger.entry(entryArgs);

    Profiler profiler = new Profiler(name);
    profiler.setLogger(extLogger);

    return profiler;
  }

}
