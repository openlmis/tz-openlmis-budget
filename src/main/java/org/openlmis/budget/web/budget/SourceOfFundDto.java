/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.budget.web.budget;

import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.openlmis.budget.domain.SourceOfFund;
import org.openlmis.budget.web.BaseDto;
import org.openlmis.budget.web.csv.model.ImportField;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public final class SourceOfFundDto extends BaseDto implements SourceOfFund.Importer,
        SourceOfFund.Exporter {
  public static final String NAME = "Name";
  public static final String CODE = "Code";
  public static final String DISPLAY_ORDER = "Display Order";

  @ImportField(name = NAME, mandatory = true)
  private String name;
  @ImportField(name = CODE, mandatory = true)
  private String code;
  @ImportField(name = DISPLAY_ORDER)
  private Integer displayOrder;

  /**
   * Creates new instance based on domain object.
   */
  public SourceOfFundDto(UUID id, String code, String name, Integer displayOrder) {
    super();
    this.id = id;
    this.code = code;
    this.name = name;
    this.displayOrder = displayOrder;
  }

  /**
   * Creates new instance based on domain object.
   */
  public static SourceOfFundDto newInstance(SourceOfFund sourceOfFund) {
    SourceOfFundDto dto = new SourceOfFundDto();
    sourceOfFund.export(dto);
    return dto;
  }
}
