/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.budget.web.budget;

import static org.openlmis.budget.i18n.MessageKeys.ERROR_FORMAT_NOT_ALLOWED;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;
import javax.servlet.http.HttpServletResponse;
import org.openlmis.budget.domain.SourceOfFund;
import org.openlmis.budget.dto.UploadResultDto;
import org.openlmis.budget.exception.ValidationMessageException;
import org.openlmis.budget.i18n.MessageKeys;
import org.openlmis.budget.repository.SourceOfFundRepository;
import org.openlmis.budget.web.BaseController;
import org.openlmis.budget.web.csv.format.CsvFormatter;
import org.openlmis.budget.web.csv.model.ModelClass;
import org.openlmis.budget.web.csv.parser.CsvParser;
import org.openlmis.budget.web.csv.recordhandler.SourceOfFundProcessor;
import org.openlmis.budget.web.csv.recordhandler.SourceOfFundWriter;
import org.openlmis.budget.web.validator.CsvHeaderValidator;
import org.slf4j.ext.XLogger;
import org.slf4j.ext.XLoggerFactory;
import org.slf4j.profiler.Profiler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.multipart.MultipartFile;

/**
 * Controller used to expose Source of fund CSV Upload via HTTP.
 */
@Controller
@RequestMapping(SourceOfFundUploadController.RESOURCE_PATH)
@Transactional
public class SourceOfFundUploadController extends BaseController {

  public static final String RESOURCE_PATH = API_PATH + "/sourceOfFunds/upload";
  private static final String PROFILER_CREATE_DTO = "PROFILER_CREATE_DTO";

  private static final XLogger XLOGGER = XLoggerFactory.getXLogger(SourceOfFundController.class);

  @Autowired
  private SourceOfFundProcessor sourceOfFundProcessor;

  @Autowired
  private SourceOfFundWriter sourceOfFundWriter;

  @Autowired
  private CsvParser csvParser;

  @Autowired
  private CsvHeaderValidator csvHeaderValidator;

  @Autowired
  private CsvFormatter csvFormatter;

  @Autowired
  private SourceOfFundRepository sourceOfFundRepository;

  /**
   * Uploads csv file and converts to domain object.
   *
   * @param file File in ".csv" format to upload.
   * @return number of uploaded records
  */
  @PostMapping(params = FORMAT)
  @ResponseBody
  @ResponseStatus(HttpStatus.OK)
  public UploadResultDto upload(@RequestParam(FORMAT) String format,
                                @RequestPart("file") MultipartFile file) {
    XLOGGER.entry(format);
    Profiler profiler = new Profiler("UPLOAD_CATALOG_ITEMS_FILE");
    profiler.setLogger(XLOGGER);

    if (!CSV.equals(format)) {
      profiler.stop().log();
      XLOGGER.exit();

      throw new ValidationMessageException(ERROR_FORMAT_NOT_ALLOWED);
    }

    profiler.start("VALIDATE");
    validateCsvFile(file);

    profiler.start("CREATE_MODEL_CLASS");
    ModelClass<SourceOfFundDto> modelClass = new ModelClass<>(SourceOfFundDto.class);

    try {

      profiler.start("PARSE_FILE");
      int result = csvParser.parse(
                file.getInputStream(), modelClass, csvHeaderValidator,
                sourceOfFundProcessor, sourceOfFundWriter
      );

      profiler.start("CREATE_RESPONSE");
      UploadResultDto uploadResult = new UploadResultDto(result);

      profiler.stop().log();
      XLOGGER.exit(uploadResult);

      return uploadResult;
    } catch (IOException ex) {
      profiler.stop().log();
      XLOGGER.exit();

      throw new ValidationMessageException(ex, MessageKeys.ERROR_IO, ex.getMessage());
    }
  }

  /**
   * Downloads csv file with all sources of fund.
  */
  @GetMapping(params = FORMAT)
  @ResponseBody
  @ResponseStatus(HttpStatus.OK)
  public void download(@RequestParam(FORMAT) String format,
                       HttpServletResponse response) throws IOException {
    XLOGGER.entry(format);
    Profiler profiler = new Profiler("DOWNLOAD_SOURCE_OF_FUND_AS_FILE");
    profiler.setLogger(XLOGGER);

    if (!CSV.equals(format)) {
      profiler.stop().log();
      XLOGGER.exit();

      throw new ValidationMessageException(ERROR_FORMAT_NOT_ALLOWED);

    }

    profiler.start("FIND_ALL");
    Iterable<SourceOfFund> items = sourceOfFundRepository.findAll();

    profiler.start(PROFILER_CREATE_DTO);
    List<SourceOfFundDto> sourceOfFundList = toDto(items);

    response.setContentType("text/csv");
    response.addHeader(HttpHeaders.CONTENT_DISPOSITION,
              DISPOSITION_BASE + "source_of_funds.csv");

    try {
      profiler.start("WRITE_TO_OUTPUT");
      csvFormatter.process(
              response.getOutputStream(),
              new ModelClass<>(SourceOfFundDto.class), sourceOfFundList);
    } catch (IOException ex) {
      throw new ValidationMessageException(ex, MessageKeys.ERROR_IO, ex.getMessage());
    } finally {
      profiler.stop().log();
      XLOGGER.exit();
    }
  }

  private SourceOfFundDto toDto(SourceOfFund sourceOfFund) {
    SourceOfFundDto dto = new SourceOfFundDto();
    sourceOfFund.export(dto);
    return dto;
  }

  private List<SourceOfFundDto> toDto(Iterable<SourceOfFund> sourceOfFunds) {
    return StreamSupport
            .stream(sourceOfFunds.spliterator(), false)
            .map(this::toDto)
            .collect(Collectors.toList());
  }

}
