/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.budget.web.budget;

import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.stream.Collectors;
import net.sf.jasperreports.engine.JRException;
import org.openlmis.budget.domain.SourceOfFund;
import org.openlmis.budget.exception.NotFoundException;
import org.openlmis.budget.exception.ValidationMessageException;
import org.openlmis.budget.i18n.MessageKeys;
import org.openlmis.budget.repository.SourceOfFundRepository;
import org.openlmis.budget.service.PermissionService;
import org.openlmis.budget.service.SourceOfFundService;
import org.openlmis.budget.util.JasperReportHelper;
import org.openlmis.budget.util.Pagination;
import org.openlmis.budget.web.BaseController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Controller used to expose Source of fund via HTTP.
 */
@Controller
@RequestMapping(SourceOfFundController.RESOURCE_PATH)
@Transactional
public class SourceOfFundController extends BaseController {
  protected static final String RESOURCE_PATH = API_PATH + "/sourceOfFunds";
  private static final Logger LOGGER = LoggerFactory.getLogger(SourceOfFundController.class);
  @Autowired
  private SourceOfFundRepository sourceOfFundRepository;

  @Autowired
  private SourceOfFundService sourceOfFundService;

  @Autowired
  private PermissionService permissionService;

  /**
   * Save a source of fund using the provided sources of fund
   * source of fund DTO. If the source of fund does not exist, will create one. If it
   * does exist, will update it.
   *
   * @param sourceOfFundDto provided source Of fund dto.
   * @return the saved source of fund dto.
   */
  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  @ResponseBody
  public SourceOfFundDto updateSourceOfFund(@RequestBody SourceOfFundDto sourceOfFundDto) {

    if (null == sourceOfFundDto.getCode() || null == sourceOfFundDto.getName()) {
      throw new ValidationMessageException(MessageKeys.ERROR_CODE_OR_NAME_REQUIRED);
    }

    LOGGER.debug("source of fund to save");
    SourceOfFund newSourceOfFundToStore = SourceOfFund.newInstance(sourceOfFundDto);

    SourceOfFund storedSourceOfFund = sourceOfFundRepository
        .findFirstByCode(newSourceOfFundToStore.getCode());

    if (storedSourceOfFund != null) {
      LOGGER.debug("Source of fund is found in the system, assign id");
      newSourceOfFundToStore.setId(storedSourceOfFund.getId());
    }
    newSourceOfFundToStore = sourceOfFundRepository
        .saveAndFlush(newSourceOfFundToStore);

    return SourceOfFundDto.newInstance(newSourceOfFundToStore);
  }

  /**
   * Updates the specified source of fund.
   */
  @PutMapping(value = "/{id}")
  @ResponseStatus(HttpStatus.OK)
  @ResponseBody
  public SourceOfFundDto saveSourceOfFund(@PathVariable("id") UUID sourceOfFundId,
                                          @RequestBody SourceOfFundDto sourceOfFundDto) {
    if (null != sourceOfFundDto.getId() && !Objects.equals(sourceOfFundDto.getId(),
        sourceOfFundId)) {
      throw new ValidationMessageException(MessageKeys.ERROR_ID_MISMATCH);
    }

    SourceOfFund newSourceOfFund =
        SourceOfFund.newInstance(sourceOfFundDto);
    SourceOfFund sourceOfFundToUpdate = sourceOfFundRepository
        .findById(sourceOfFundId)
        .orElse(null);

    SourceOfFund sourceOfFundToSave;
    if (sourceOfFundToUpdate == null) {
      sourceOfFundToSave = newSourceOfFund;
      sourceOfFundToSave.setId(sourceOfFundId);
    } else {
      sourceOfFundToSave = sourceOfFundToUpdate;
      sourceOfFundToSave.updateFrom(sourceOfFundDto);
    }

    LOGGER.debug("Updating Sources of fund");

    sourceOfFundToSave = sourceOfFundRepository
        .saveAndFlush(sourceOfFundToSave);
    return SourceOfFundDto.newInstance(sourceOfFundToSave);
  }

  /**
   * Retrieves all source of funds. Note that an empty collection rather than a 404 should be
   * returned if no source of funds exist.
   */
  @GetMapping
  @ResponseStatus(HttpStatus.OK)
  @ResponseBody
  public Page<SourceOfFundDto> getAllSourceOfFunds(Pageable pageable) {
    Page<SourceOfFund> page = sourceOfFundRepository.findAll(pageable);
    List<SourceOfFundDto> content = page
        .getContent()
        .stream()
        .map(SourceOfFundDto::newInstance)
        .collect(Collectors.toList());
    return Pagination.getPage(content, pageable, page.getTotalElements());
  }

  /**
   * Retrieves the specified source of fund.
   */
  @GetMapping(value = "/{id}")
  @ResponseStatus(HttpStatus.OK)
  @ResponseBody
  public SourceOfFundDto getSpecifiedSourceOfFund(@PathVariable("id") UUID id) {
    SourceOfFund sourceOfFund = sourceOfFundRepository.findById(id)
        .orElseThrow(() -> new NotFoundException(MessageKeys.ERROR_SOURCE_OF_FUND_NOT_FOUND));
    return SourceOfFundDto.newInstance(sourceOfFund);
  }

  /**
   * Retrieves audit information related to the specified source of fund.
   *
   * @param author              The author of the changes which should be returned.
   *                            If null or empty, changes are returned regardless of author.
   * @param changedPropertyName The name of the property about which changes should be returned.
   *                            If null or empty, changes associated with any and all properties
   *                            are returned.
   * @param page                A Pageable object that allows client to optionally
   *                            add "page" (page number)
   *                            and "size" (page size) query parameters to the request.
   */
  @GetMapping(value = "/{id}/auditLog")
  @ResponseStatus(HttpStatus.OK)
  @ResponseBody
  public ResponseEntity<String> getSourceOfFundAuditLog(@PathVariable("id") UUID id,
                                                        @RequestParam(name = "author",
                                                            required = false,
                                                            defaultValue = "") String author,
                                                        @RequestParam(name = "changedPropertyName",
                                                            required = false, defaultValue = "")
                                                            String changedPropertyName,
                                                        Pageable page) {

    //Return a 404 if the specified instance can't be found
    if (!sourceOfFundRepository.existsById(id)) {
      throw new NotFoundException(MessageKeys.ERROR_SOURCE_OF_FUND_NOT_FOUND);
    }

    return getAuditLogResponse(SourceOfFund.class, id, author, changedPropertyName, page);
  }

  /**
   * Generate funds source summary report for the requisitions submitted in the specified zone.
   *
   * @param programId filter for program requisitions
   * @param periodId  filter for period requisitions
   * @param zoneId    filter for zone requisitions
   * @param format    the format for generating reports
   */
  @RequestMapping(value = "/zoneSummaries/{format}", method = RequestMethod.GET)
  @ResponseBody
  public ResponseEntity<byte[]> generateZoneSourceOfFundSummary(@RequestParam(required = false)
                                                                    UUID programId,
                                                                @RequestParam(required = false)
                                                                    UUID periodId,
                                                                @RequestParam UUID zoneId,
                                                                @PathVariable("format")
                                                                    String format)
      throws JRException, ClassNotFoundException {

    permissionService.canViewReports();
    byte[] reportData = sourceOfFundService.generateZoneFundSourceSummaryReport(
        programId,
        periodId,
        zoneId,
        format
    );

    return ResponseEntity
        .ok()
        .contentType(JasperReportHelper.resolveMediaType(format))
        .header(
            "Content-Disposition",
            "inline; filename=Source of funds summary." + format
        ).body(reportData);
  }

  /**
   * Generate funds source summary report for the requisitions submitted in the specified facility.
   *
   * @param programId  filter for program requisitions
   * @param periodId   filter for period requisitions
   * @param facilityId filter for zone requisitions
   * @param format     the format for generating reports
   */
  @RequestMapping(value = "/facilitySummaries/{format}", method = RequestMethod.GET)
  @ResponseBody
  public ResponseEntity<byte[]> generateFacilitySourceOfFundSummary(@RequestParam(required = false)
                                                                        UUID programId,
                                                                    @RequestParam(required = false)
                                                                        UUID periodId,
                                                                    @RequestParam UUID facilityId,
                                                                    @PathVariable("format")
                                                                        String format)
      throws JRException, ClassNotFoundException {
    permissionService.canViewReports();

    byte[] reportData = sourceOfFundService.generateFacilityFundSourceSummaryReport(
        programId,
        periodId,
        facilityId, format
    );
    return ResponseEntity
        .ok()
        .contentType(JasperReportHelper.resolveMediaType(format))
        .header(
            "Content-Disposition",
            "inline; filename=Source of funds summary." + format
        ).body(reportData);
  }
}
