/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.budget.web.budget;

import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.stream.Collectors;
import org.openlmis.budget.domain.RequisitionBudget;
import org.openlmis.budget.exception.NotFoundException;
import org.openlmis.budget.exception.ValidationMessageException;
import org.openlmis.budget.i18n.MessageKeys;
import org.openlmis.budget.repository.RequisitionBudgetRepository;
import org.openlmis.budget.util.Pagination;
import org.openlmis.budget.web.BaseController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Controller used to expose budget requisition via HTTP.
 */
@Controller
@RequestMapping(RequisitionBudgetController.RESOURCE_PATH)
@Transactional
public class RequisitionBudgetController extends BaseController {

  private static final Logger LOGGER = LoggerFactory.getLogger(RequisitionBudgetController.class);

  public static final String RESOURCE_PATH = API_PATH + "/requisitionBudgets";

  @Autowired
  private RequisitionBudgetRepository requisitionBudgetRepository;

  /**
   * Allows the creation of a new requisition budget. If the id is specified, it will be ignored.
   */
  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  @ResponseBody
  public RequisitionBudgetDto createRequisitionBudget(@RequestBody
                                                     RequisitionBudgetDto requisitionBudgetDto) {

    if (null == requisitionBudgetDto.getRequisitionId()
            || null == requisitionBudgetDto.getSourceOfFundId()) {
      throw new ValidationMessageException(MessageKeys.MUST_CONTAIN_VALUE);
    }

    LOGGER.debug("Creating new requisition budget");
    RequisitionBudget newRequisitionBudget = RequisitionBudget.newInstance(requisitionBudgetDto);

    RequisitionBudget storedRequisitionBudget = requisitionBudgetRepository
            .findById(requisitionBudgetDto.getId()).orElse(null);

    if (storedRequisitionBudget != null) {
      LOGGER.debug("requisition budget in the system, assign id");
      newRequisitionBudget.setId(storedRequisitionBudget.getId());
    }
    newRequisitionBudget = requisitionBudgetRepository
            .saveAndFlush(newRequisitionBudget);

    return RequisitionBudgetDto.newInstance(newRequisitionBudget);
  }

  /**
   * Updates the specified requisition budget.
   */
  @PutMapping(value = "/{id}")
  @ResponseStatus(HttpStatus.OK)
  @ResponseBody
  public RequisitionBudgetDto saveRequisitionBudget(@PathVariable("id") UUID id,
                                          @RequestBody RequisitionBudgetDto requisitionBudgetDto) {
    if (null != requisitionBudgetDto.getId() && !Objects.equals(requisitionBudgetDto.getId(), id)) {
      throw new ValidationMessageException(MessageKeys.ERROR_BUDGET_ID_MISMATCH);
    }

    LOGGER.debug("Updating requisition budget");

    RequisitionBudget newRequisitionBudget =
            RequisitionBudget.newInstance(requisitionBudgetDto);

    RequisitionBudget requisitionBudgetToUpdate = requisitionBudgetRepository
            .findById(id)
            .orElse(null);

    RequisitionBudget requisitionBudgetToSave;
    if (requisitionBudgetToUpdate == null) {
      requisitionBudgetToSave = newRequisitionBudget;
      requisitionBudgetToSave.setId(id);
    } else {
      requisitionBudgetToSave = requisitionBudgetToUpdate;
      requisitionBudgetToSave.updateFrom(requisitionBudgetDto);
    }

    requisitionBudgetRepository.saveAndFlush(requisitionBudgetToSave);

    return RequisitionBudgetDto.newInstance(requisitionBudgetToSave);
  }

  /**
   * Retrieves all requisition budgets. Note that an empty collection rather than a 404 should be
   * returned if no requisition budget exist.
   */
  @GetMapping
  @ResponseStatus(HttpStatus.OK)
  @ResponseBody
  public Page<RequisitionBudgetDto> getAllRequisitionBudgets(Pageable pageable) {
    Page<RequisitionBudget> page = requisitionBudgetRepository.findAll(pageable);
    List<RequisitionBudgetDto> content = page
        .getContent()
        .stream()
        .map(RequisitionBudgetDto::newInstance)
        .collect(Collectors.toList());
    return Pagination.getPage(content, pageable, page.getTotalElements());
  }

  /**
   * Retrieves the specified requisition budgets.
   */
  @GetMapping(value = "/{id}")
  @ResponseStatus(HttpStatus.OK)
  @ResponseBody
  public RequisitionBudgetDto getSpecifiedRequisitionBudget(@PathVariable("id") UUID id) {
    RequisitionBudget requisitionBudget = requisitionBudgetRepository.findById(id)
        .orElseThrow(() -> new NotFoundException(MessageKeys.ERROR_REQUISITION_BUDGET_NOT_FOUND));

    return RequisitionBudgetDto.newInstance(requisitionBudget);
  }

  /**
   * Retrieves audit information related to the specified requisition budget.
   *
   * @param author The author of the changes which should be returned.
   *               If null or empty, changes are returned regardless of author.
   * @param changedPropertyName The name of the property about which changes should be returned.
   *               If null or empty, changes associated with any and all properties are returned.
   * @param page A Pageable object that allows client to optionally add "page" (page number)
   *             and "size" (page size) query parameters to the request.
   */
  @GetMapping(value = "/{id}/auditLog")
  @ResponseStatus(HttpStatus.OK)
  @ResponseBody
  public ResponseEntity<String> getRequisitionBudgetAuditLog(@PathVariable("id") UUID id,
      @RequestParam(name = "author", required = false, defaultValue = "") String author,
      @RequestParam(name = "changedPropertyName", required = false, defaultValue = "")
          String changedPropertyName, Pageable page) {

    //Return a 404 if the specified instance can't be found
    if (!requisitionBudgetRepository.existsById(id)) {
      throw new NotFoundException(MessageKeys.ERROR_REQUISITION_BUDGET_NOT_FOUND);
    }

    return getAuditLogResponse(RequisitionBudget.class, id, author, changedPropertyName, page);
  }

}
