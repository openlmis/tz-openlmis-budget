/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.budget.web.budget;

import java.math.BigDecimal;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.openlmis.budget.domain.Budget;
import org.openlmis.budget.web.BaseDto;
import org.openlmis.budget.web.csv.model.ImportField;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public final class BudgetDto extends BaseDto implements Budget.Importer,
        Budget.Exporter {

  static final String FACILITY_CODE = "Facility Code";
  static final String PROGRAM_CODE = "Program Code";
  static final String SOURCE_OF_FUND_CODE = "Source Of Fund Code";
  static final String LAST_UPDATED_DATE = "Last Updated Date";
  static final String NOTE = "Note";
  static final String BUDGET_AMOUNT = "Budget Amount";

  private UUID facilityId;
  private UUID programId;
  private UUID sourceOfFundId;

  @ImportField(name = FACILITY_CODE, mandatory = true)
  private String facilityCode;

  @ImportField(name = PROGRAM_CODE)
  private String programCode;

  @ImportField(name = SOURCE_OF_FUND_CODE, mandatory = true)
  private String sourceOfFundCode;

  @ImportField(name = NOTE)
  private String note;

  @ImportField(name = BUDGET_AMOUNT, mandatory = true)
  private BigDecimal budgetAmount;

  private String sourceOfFundName;

  /**
   * Creates new instance based on domain object.
   */
  public static BudgetDto newInstance(Budget budget) {
    BudgetDto dto = new BudgetDto();
    budget.export(dto);
    return dto;
  }
}
