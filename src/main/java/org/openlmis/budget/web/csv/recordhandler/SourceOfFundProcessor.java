/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.budget.web.csv.recordhandler;

import org.openlmis.budget.domain.SourceOfFund;
import org.openlmis.budget.validate.SourceOfFundValidator;
import org.openlmis.budget.web.budget.SourceOfFundDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * SourceOfFundProcessor is used for uploads of Source Of Fund.
 * It uploads each source of fund record by
 * record.
 */
@Component
public class SourceOfFundProcessor implements RecordProcessor<SourceOfFundDto, SourceOfFund> {

  @Autowired
  private SourceOfFundValidator sourceOfFundValidator;

  @Override
  public SourceOfFund process(SourceOfFundDto dto) {
    sourceOfFundValidator.validateExistingSourceOfFund(dto);
    return SourceOfFund.newInstance(dto);
  }
}