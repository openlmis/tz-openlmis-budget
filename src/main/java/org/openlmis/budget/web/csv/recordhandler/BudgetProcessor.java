/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.budget.web.csv.recordhandler;

import org.openlmis.budget.domain.Budget;
import org.openlmis.budget.validate.BudgetValidator;
import org.openlmis.budget.web.budget.BudgetDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * BudgetProcessor is used for uploads of Budget Allocations.
 * It uploads each source of fund record by
 * record.
 */
@Component
public class BudgetProcessor implements RecordProcessor<BudgetDto, Budget> {

  @Autowired
  private BudgetValidator budgetValidator;

  @Override
  public Budget process(BudgetDto dto) {
    budgetValidator.validateExistingBudget(dto);
    return Budget.newInstance(dto);
  }

}
