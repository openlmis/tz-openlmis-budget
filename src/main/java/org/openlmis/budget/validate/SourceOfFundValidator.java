/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.budget.validate;

import static org.openlmis.budget.i18n.MessageKeys.ERROR_FROM_FIELD_REQUIRED;

import java.util.UUID;
import org.openlmis.budget.exception.ValidationMessageException;
import org.openlmis.budget.i18n.MessageKeys;
import org.openlmis.budget.web.budget.SourceOfFundDto;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

/**
 * A validator for {@link org.openlmis.budget.web.budget.SourceOfFundDto} object.
 */
@Component
public class SourceOfFundValidator implements BaseValidator {

  // Source of fund fields
  static final String CODE = "code";
  static final String NAME = "name";

  /**
   * Checks if the given class definition is supported.
   *
   * @param clazz the {@link Class} that is being asked if it can {@link
   *              #validate(Object, Errors) validate}
   * @return true if {@code clazz} is equal to {@link SourceOfFundDto} class definition.
   *     Otherwise false.
   */
  @Override
  public boolean supports(Class<?> clazz) {
    return SourceOfFundDto.class.equals(clazz);
  }

  /**
   * Validates the {@code target} object, which must be an instance of
   * {@link SourceOfFundDto} class.
   * <p>Firstly, the method checks if the target object has a value in {@code code} and {@code name}
   * properties. For those two properties the value cannot be {@code null}, empty or
   * contains only whitespaces.</p>
   * <p>If there are no errors, the method checks if the {@code code} property in the target
   * object is unique.</p>
   *
   * @param target the object that is to be validated (never {@code null})
   * @param errors contextual state about the validation process (never {@code null})
   */
  @Override
  public void validate(Object target, Errors errors) {
    verifyArguments(target, errors, MessageKeys.ERROR_NULL);
    rejectIfEmptyOrWhitespace(errors, CODE, MessageKeys.ERROR_CODE_OR_NAME_REQUIRED);
    rejectIfEmptyOrWhitespace(errors, NAME, MessageKeys.ERROR_CODE_OR_NAME_REQUIRED);
    verifyProperties(errors);
  }

  private void verifyProperties(Errors errors) {
    // the code is required
    rejectIfEmptyOrWhitespace(errors, CODE, MessageKeys.ERROR_CODE_REQUIRED);
  }

  /**
   * Validate source of fund that will be added.
   * @param sourceOfFund the object that is to be validated (never {@code null})
   */
  public void validateExistingSourceOfFund(SourceOfFundDto sourceOfFund) {
    validateNullValues(sourceOfFund);
  }

  private boolean valuesMisMatch(SourceOfFundDto sourceOfFundDto, UUID sourceOfFundId) {
    return (!sourceOfFundDto.getId().equals(sourceOfFundId));
  }

  private void validateNullValues(SourceOfFundDto sourceOfFund) {
    validateNotNull(sourceOfFund.getCode(), ERROR_FROM_FIELD_REQUIRED, "code");
    validateNotNull(sourceOfFund.getName(), ERROR_FROM_FIELD_REQUIRED, "name");
    validateNotNull(sourceOfFund.getDisplayOrder(), ERROR_FROM_FIELD_REQUIRED, "displayOrder");
  }

  private void validateNotNull(Object field, String errorMessage, String fieldName) {
    if (field == null) {
      throw new ValidationMessageException(errorMessage, fieldName);
    }
  }

}
