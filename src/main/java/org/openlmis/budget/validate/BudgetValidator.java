/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.budget.validate;

import static org.openlmis.budget.i18n.MessageKeys.ERROR_BUDGET_ID_MISMATCH;
import static org.openlmis.budget.i18n.MessageKeys.ERROR_FROM_FIELD_REQUIRED;

import java.util.UUID;
import org.openlmis.budget.exception.ValidationMessageException;
import org.openlmis.budget.web.budget.BudgetDto;
import org.springframework.stereotype.Component;

@Component
public class BudgetValidator {

  /**
   * Validate source of fund that will be added.
   */
  public void validateExistingBudget(BudgetDto budgetDto) {

    validateNullValues(budgetDto);

  }

  /**
   * Validate mismatch and already existing budget.
   */
  public void validateMisMatchBudget(BudgetDto budgetDto, UUID budgetId) {
    boolean existing = valuesMisMatch(budgetDto, budgetId);
    if (existing) {
      throw new ValidationMessageException(ERROR_BUDGET_ID_MISMATCH, budgetId);
    } else {
      validateExistingBudget(budgetDto);
    }
  }

  private boolean valuesMisMatch(BudgetDto budgetDto, UUID budgetId) {

    return (!budgetDto.getId().equals(budgetId));

  }

  private void validateNullValues(BudgetDto budgetDto) {
    validateNotNull(budgetDto.getFacilityCode(),
            ERROR_FROM_FIELD_REQUIRED, "facilityCode");
  }

  private void validateNotNull(Object field, String errorMessage, String fieldName) {
    if (field == null) {
      throw new ValidationMessageException(errorMessage, fieldName);
    }
  }
}
