/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.budget.util;

import java.nio.charset.StandardCharsets;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperPrint;
import org.springframework.http.MediaType;

public class JasperReportHelper {

  /**
   * Export jasper report to the specified format.
   *
   * @param jasperPrint a Jasper print to be exported to the specified format.
   * @param format the format for the report
   */
  public static byte[] exportJasperReport(JasperPrint jasperPrint, String format)
      throws JRException {
    if ("csv".equals(format)) {
      return exportJasperReportToCsv(jasperPrint);
    } else if ("xls".equals(format)) {
      return exportJasperReportToXls(jasperPrint);
    } else if ("html".equals(format)) {
      return exportJasperReportToHtml(jasperPrint);
    } else if ("pdf".equals(format)) {
      return exportJasperReportToPdf(jasperPrint);
    } else {
      throw new IllegalArgumentException("Format of type " + format + " is not supported");
    }
  }

  /**
   * Resolve the media type for the response based on the specified format.
   *
   * @param format the format of the content.
   */
  public static MediaType resolveMediaType(String format) {
    if ("csv".equals(format)) {
      return new MediaType("text", "csv", StandardCharsets.UTF_8);
    } else if ("xls".equals(format)) {
      return new MediaType("application", "vnd.ms-excel", StandardCharsets.UTF_8);
    } else if ("html".equals(format)) {
      return new MediaType("text", "html", StandardCharsets.UTF_8);
    } else if ("pdf".equals(format)) {
      return new MediaType("application", "pdf", StandardCharsets.UTF_8);
    } else {
      throw new IllegalArgumentException("Format of type " + format + " is not supported");
    }
  }

  /**
   * Export Jasper report to CSV.
   *
   * @param jasperPrint a Jasper print to be exported to the specified format.
   */
  public static byte[] exportJasperReportToCsv(JasperPrint jasperPrint) throws JRException {
    return new JasperCsvExporter(jasperPrint).exportReport();
  }

  /**
   * Export Jasper report to XLS.
   *
   * @param jasperPrint a Jasper print to be exported to the specified format.
   */
  public static byte[] exportJasperReportToXls(JasperPrint jasperPrint) throws JRException {
    return new JasperXlsExporter(jasperPrint).exportReport();
  }

  /**
   * Export Jasper report to HTML.
   *
   * @param jasperPrint a Jasper print to be exported to the specified format.
   */
  public static byte[] exportJasperReportToHtml(JasperPrint jasperPrint) throws JRException {
    return new JasperHtmlExporter(jasperPrint).exportReport();
  }

  /**
   * Export Jasper report to PDF.
   *
   * @param jasperPrint a Jasper print to be exported to the specified format.
   */
  public static byte[] exportJasperReportToPdf(JasperPrint jasperPrint) throws JRException {
    return JasperExportManager.exportReportToPdf(jasperPrint);
  }

}
