/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.budget.domain;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.javers.core.metamodel.annotation.TypeName;

@SuppressWarnings("PMD.TooManyMethods")
@Entity
@TypeName("SourceOfFund")
@Table(name = "source_of_funds", schema = "budget")
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class SourceOfFund extends BaseEntity {

  @Column(unique = true, columnDefinition = TEXT_COLUMN_DEFINITION)
  @Getter
  @Setter
  private String code;

  @Column(unique = true, columnDefinition = TEXT_COLUMN_DEFINITION)
  @Getter
  @Setter
  private String name;

  @Column(nullable = false, columnDefinition = INTEGER_COLUMN_DEFINITION)
  @Getter
  @Setter
  private Integer displayOrder;

  /**
   * Copy values of attributes into new or updated source of funds.
   *
   * @param code            source of funds code with new values.
   * @param name            source of funds name with new values.
   * @param displayOrder    source of funds displayOrder with new values.
   */
  public SourceOfFund(String code, String name, Integer displayOrder) {
    this.code = code;
    this.name = name;
    this.displayOrder = displayOrder;
  }

  /**
   * Creates new instance based on data from {@link Importer}.
   *
   * @param importer instance of {@link Importer}
   * @return new instance of Source Of Fund.
   */
  public static SourceOfFund newInstance(Importer importer) {

    SourceOfFund sourceOfFund = new SourceOfFund();
    sourceOfFund.setId(importer.getId());
    sourceOfFund.updateFrom(importer);
    return sourceOfFund;
  }

  /**
   * a method that set values for updates .
   */
  public void updateFrom(Importer importer) {
    name = importer.getName();
    code = importer.getCode();
    displayOrder = importer.getDisplayOrder();
  }

  /**
   * Creates list of new instances based on data from {@link Importer} list.
   *
   * @param importers {@link Importer} list
   * @return list of new SourceOfFund instances.
  */
  public static List<SourceOfFund> newInstanceList(List<? extends Importer> importers) {
    return importers.stream()
           .map(SourceOfFund::newInstance)
           .collect(Collectors.toList());
  }

  /**
     * Creates new SourceOfFund object based on data from {@link SourceOfFund.Importer}.
     *
     * @param importer instance of {@link SourceOfFund.Importer}
     * @return new instance of SourceOfFund.
  */
  public static SourceOfFund newSourceOfFund(Importer importer) {

    SourceOfFund sourceOfFund = new SourceOfFund(importer.getCode(),
                                importer.getName(), importer.getDisplayOrder());
    sourceOfFund.id = importer.getId();
    return sourceOfFund;

  }

  /**
    * Export this object to the specified exporter (DTO).
    *
    * @param exporter exporter to export to
  */
  public void export(SourceOfFund.Exporter exporter) {

    exporter.setId(id);
    exporter.setCode(code);
    exporter.setName(name);
    exporter.setDisplayOrder(displayOrder);
  }

  public interface Importer {

    UUID getId();

    String getCode();

    String getName();

    Integer getDisplayOrder();

  }

  public interface Exporter {

    void setCode(String code);

    void setDisplayOrder(Integer displayOrder);

    void setId(UUID id);

    void setName(String name);
  }

}
