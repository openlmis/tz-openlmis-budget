/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.budget.domain;

import java.math.BigDecimal;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.Type;
import org.javers.core.metamodel.annotation.TypeName;

@SuppressWarnings("PMD.TooManyMethods")
@Entity
@TypeName("Budget")
@Table(name = "budgets", schema = "budget")
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class Budget extends BaseEntity {

  @Column(nullable = false)
  @Getter
  @Setter
  @Type(type = UUID_TYPE)
  private UUID facilityId;

  @Column(nullable = false)
  @Getter
  @Setter
  @Type(type = UUID_TYPE)
  private UUID programId;

  @Column(nullable = false)
  @Getter
  @Setter
  @Type(type = UUID_TYPE)
  private UUID sourceOfFundId;

  @Getter
  @Setter
  private BigDecimal budgetAmount;

  @Getter
  @Setter
  @Type(type = TEXT_COLUMN_DEFINITION)
  private String note;

  @Getter
  @Setter
  @Transient
  private transient String sourceOfFundCode;

  @Getter
  @Setter
  @Transient
  private transient String programCode;

  @Getter
  @Setter
  @Transient
  private transient String facilityCode;

  /**
     * Creates new instance based on data from {@link Importer}.
     *
     * @param importer instance of {@link Importer}
     * @return new instance of Budget.
  */
  public static Budget newInstance(Importer importer) {
    Budget budget = new Budget();
    budget.setId(importer.getId());
    budget.updateFrom(importer);
    return budget;
  }

  /**
    * a method that set values for updates .
  */
  public void updateFrom(Importer importer) {
    facilityId = importer.getFacilityId();
    programId = importer.getProgramId();
    sourceOfFundId = importer.getSourceOfFundId();
    budgetAmount = importer.getBudgetAmount();
    note = importer.getNote();
    sourceOfFundCode = importer.getSourceOfFundCode();
    programCode = importer.getProgramCode();
    facilityCode = importer.getFacilityCode();
  }

  /**
    * Export this object to the specified exporter (DTO).
    *
    * @param exporter exporter to export to
  */
  public void export(Exporter exporter) {
    exporter.setId(id);

    exporter.setFacilityId(facilityId);

    exporter.setProgramId(programId);

    exporter.setBudgetAmount(budgetAmount);

    exporter.setSourceOfFundId(sourceOfFundId);

    exporter.setNote(note);

    exporter.setProgramCode(programCode);

    exporter.setSourceOfFundCode(sourceOfFundCode);

    exporter.setFacilityCode(facilityCode);
  }

  public interface Importer {

    UUID getId();

    UUID getFacilityId();

    UUID getProgramId();

    UUID getSourceOfFundId();

    BigDecimal getBudgetAmount();

    String getNote();

    String getFacilityCode();

    String getSourceOfFundCode();

    String getProgramCode();

  }

  public interface Exporter {

    void setId(UUID id);

    void setFacilityId(UUID facilityId);

    void setProgramId(UUID programId);

    void setSourceOfFundId(UUID sourceOfFundId);

    void setBudgetAmount(BigDecimal budgetAmount);

    void setNote(String note);

    void setSourceOfFundCode(String sourceOfFundCode);

    void setProgramCode(String programCode);

    void setFacilityCode(String facilityCode);

  }


}
