/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.budget.domain;

import java.math.BigDecimal;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.Type;
import org.javers.core.metamodel.annotation.TypeName;

@SuppressWarnings("PMD.TooManyMethods")
@Entity
@TypeName("Budget")
@Table(name = "requisition_budgets", schema = "budget")
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class RequisitionBudget extends BaseEntity {

  @Column(nullable = false)
  @Getter
  @Setter
  @Type(type = UUID_TYPE)
  private UUID requisitionId;

  @Column(nullable = false)
  @Getter
  @Setter
  @Type(type = UUID_TYPE)
  private UUID sourceOfFundId;

  @Getter
  @Setter
  private BigDecimal budgetAmount;

  /**
     * Creates new instance based on data from {@link RequisitionBudget.Importer}.
     *
     * @param importer instance of {@link RequisitionBudget.Importer}
     * @return new instance of Budget.
  */
  public static RequisitionBudget newInstance(RequisitionBudget.Importer importer) {
    RequisitionBudget budget = new RequisitionBudget();
    budget.setId(importer.getId());
    budget.updateFrom(importer);
    return budget;
  }

  /**
    * a method that set values for updates .
  */
  public void updateFrom(Importer importer) {
    sourceOfFundId = importer.getSourceOfFundId();
    budgetAmount = importer.getBudgetAmount();
    requisitionId = importer.getRequisitionId();
  }

  /**
    * Export this object to the specified exporter (DTO).
    *
    * @param exporter exporter to export to
  */
  public void export(RequisitionBudget.Exporter exporter) {

    exporter.setId(id);

    exporter.setRequisitionId(requisitionId);

    exporter.setSourceOfFundId(sourceOfFundId);

    exporter.setBudgetAmount(budgetAmount);

  }

  public interface Importer {

    UUID getId();

    UUID getRequisitionId();

    UUID getSourceOfFundId();

    BigDecimal getBudgetAmount();

  }

  public interface Exporter {

    void setId(UUID id);

    void setRequisitionId(UUID requisitionId);

    void setSourceOfFundId(UUID sourceOfFundId);

    void setBudgetAmount(BigDecimal budgetAmount);

  }

}
