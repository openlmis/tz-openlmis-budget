/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.budget.dto.requisition;

import java.time.ZonedDateTime;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.openlmis.budget.dto.BaseDto;
import org.openlmis.budget.dto.referencedata.FacilityDto;
import org.openlmis.budget.dto.referencedata.OrderableDto;
import org.openlmis.budget.dto.referencedata.ProgramDto;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class RequisitionDto extends BaseDto {

  private ZonedDateTime createdDate;

  private ZonedDateTime modifiedDate;

  private RequisitionStatus status;

  private Boolean emergency;

  private UUID supervisoryNode;

  private Map<String, Object> extraData;
  private FacilityDto facility;

  private ProgramDto program;
  private ProcessingPeriodDto processingPeriod;

  private Set<OrderableDto> availableFullSupplyProducts;

  private Set<OrderableDto> availableNonFullSupplyProducts;
}
